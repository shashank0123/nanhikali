<?php
return [
	"home" => "HOME",
	"about" => "ABOUT US",
	"causes" => "CAUSES",
	"events" => "EVENTS",
	"blog" => "BLOG",
	"gallery" => "GALLERY",
	"award" => "AWARDS & ACHIEVEMENTS",
	"faq" => "FAQ",
	"team" => "OUR TEAM",
	"contact" => "CONTACT US",
	"donate" => "DONATE NOW",
	"login" => "LOGIN",
	"we" => "We",
	"need" => "Need",
	"your_help" => "Your Help",
	"please" => "Please come forward and contribute. Your contribution will transform lifes of many .",
	"disaster" => "DISASTER RECOVERY",
	"strike" => "Disasters strike the countrywith regular frequency, causing massive human and economic ,losses.Along ..",
	"to" => "to go",
	"girls" => "GIRLS EDUCATION",
	"education" => "Education is considered as the panacea for all the ills prevailing in the society. Education ,is the ..",
	"women" => "WOMEN EMPOWERMENT",
	"financial" => "The financial growth of the kingdom at some stage in the beyond few many years has been ,exceptional...",
	"cause" => "SEE ALL CAUSE",
	"donated" => "Donated",
	"how" => "How To Us",
	"help" => "Help",
	"us" => "Us",
	"participatory" => "Project Nanhi Kali is a participatory project where you can sponsor the education of an ,underprivileged girl child. You can sponsor a Nanhi Kali from primary school studying in class 1-5 at just Rs. 3600 a year, while for Rs. 4800 you can sponsor a Nanhi Kali from secondary school studying in class 6-10. Thereafter, in the first year, you will receive the photograph, profile and progress report of the Nanhi Kali your support, so you are updated on how she is faring in both academics as well as extracurricular activities.",
	"send" => "Send Donation",
	"contribute" => "Please come forward and contribute. Your contribution will transform lives of many.",
	"become" => "Become Volunteer",
	"sponsor" => "You can sponsor the education of one or more underprivileged girls. You will have the option to ,choose the number and education level of the girls you are sponsoring.",
	"share" => "Share Media",
	"increase" => "Increase our followers on Twitter, Expand our network on Facebook, Write stories and updates ,for our Blog & Follow us on Instagram.",
	"many" => "We Help Many People",
	 "people" => "People",
	"want" => "Want To Become a Volunteer",
	"volunteers" => "Volunteers do not necessarily have the time; they just have the heart. You can sponsor the ,education of one or more underprivileged girls. You will have the option to choose the number and education level of the girls you are sponsoring.",
	"learn" => "LEARN MORE",
	"join" => "JOIN US NOW",
	"ou" => "Our",
	"says" => "Our Volunteers Says",
	"Volunt" => "Volunteers",
	"earlier" => "Earlier the parents sent their girls to primary school but didn�t show much interest in High ,school. Nowadays, in a family if all the kids are going to school, it is like a competition that has emerged amongst all the neighbors and looking at other families sending their kids to school, the parents are encouraged to do the same. Before I joined as a tutor, if the kids had any doubts, it would remain unanswered because the parents couldn�t understand as they were illiterate. Now that I�m there, I make it a point that I�m more of a friend than a teacher. I wish all my students understand the subjects.",
	"rajita" => "Rajita",
	"part" => "Part Of Project Nanhi Kali team",
	"pleased" => "We are very pleased to support Project Nanhi Kali because it addresses the basic issue of health ,and education of underprivileged girl children. The focus of this project aligns very well with one of the principles of our credo: our commitment of responsibility to the communities in which we live and work, and to the world community as well.  The implementation of this project is done in a very professional way with passion and commitment. When J&J employees see the smiling faces of little girls,  it makes us very happy and proud!",
	"ram" => "Ram Vaidya",
	"regional" => "Regional Product Development Director, Johnson & Johnson India",
	"working" => "Working in the field of responsible tourism gives me the opportunity to see many interesting ,projects worldwide. Though, would I have to pick my favourite project now, that would be the Project Nanhi Kali that helps the  poor girls by giving them opportunity to complete their education and fulfil their dreams. ",
	"nishant" => "Nishant",
	"founder" => "Business Founder",
	"enjoyed" => "Enjoyed the visit and glad it finally happened and I could see all the good work being done in ,person and to see so many bright sparks amongst the girl students, enthusiastic & confident to stand up and speak in front of new people.",
	"usha" => "Usha",
	"educate" => "�Educate Girls is the most cost-effective education intervention working at a significant scale ,we have found � and we are happy to partner with them.�",
	"sam" => "Sam Morgan",
	"fantastic" => "�Nanhi Kali is a fantastic NGO. It is already exceeding its targets in providing and ,supporting quality primary education for girls and addressing the critical issue of retention. We look forward to seeing Nanhi Kali grow and thrive in the coming years.",
	"neera" => "Neera Nundy",
	"cofounder" => "Partner and Co-Founder, Dasra",
	"our" => "Our Gallery",
	"catch" => "Catch a glimpse of the candid and innocent moments in the lives of our Nanhi Kalis.",
	"upcoming" => "Upcoming Events",
	"give" => "Give your precious time to our Nanhi Kalis by becoming part of our upcoming events.",
	"partner" => "Our Partner",
	"hand" => "Join your hand with us for a better life and bright future.",
	"who" => "WHO WE ARE",
	"where" => "WHERE WE WORK",
	"us" => "FOLLOW US",
	"project" => "Project Nanhi Kali is an Indian Non-Governmental Organization that supports education for ,under-privileged girls in India.",
	"subscribe" => "Subscribe Here For Newsletter",
	"submit" => "Submit",
	"enter" => "Enter your Email",
	"more" => "Read More",
];
