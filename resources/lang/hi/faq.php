<?php
return[
"frequently" => "Frequently Asked Question",
"how" => "How To Help Us",
"project" => "Project Nanhi Kali is a participatory project where you can sponsor the education of an underprivileged girl child. You can sponsor a Nanhi Kali from primary school studying in class 1-5 at just Rs. 3600 a year, while for Rs. 4800 you can sponsor a Nanhi Kali from secondary school studying in class 6-10. Thereafter, in the first year, you will receive the photograph, profile and progress report of the Nanhi Kali your support, so you are updated on how she is faring in both academics as well as extracurricular activities.",
"send" => "Send Donation",
"contribute" => "Please come forward and contribute. Your contribution will transform lives of many.",
"become" => "Become Volunteer",
"sponsor" => "You can sponsor the education of one or more underprivileged girls. You will have the option to choose the number and education level of the girls you are sponsoring.",
"share" => "Share Media",
"increase" => "Increase our followers on Twitter, Expand our network on Facebook, Write stories and updates for our Blog & Follow us on Instagram.",
];