<?php
return [
	"about" => "About Us",
	"nanhi" => "The Nanhi Kali project was started to provide necessary training to the oppressed girls between the age of 5 to 15 years in India.",
	"project" => "Project Nanhi Kali was founded with a firm conviction, which informs people that education, is very important for girls. Through this project, all girls in the country will get a complete education. The girl child education in India is necessary for the growth of the country to a large extent because girls can do a better job than boys. Nowadays girl child education is necessary and it is also mandatory because they are the future of the country. Girl child education is necessary to develop India socially and economically. An educated girl has made a positive impact on Indian society by their contribution to professional fields such as medicine, defense services, science, and technology. Girls can do business well and know how to handle their home and office well. A better economy and a better society is the result of the education of girls. An educated girl can help to control the country's population by marrying at the right time or later than uneducated women. Our mission is to mobilize and motivate non-school going girls between the ages of 5 and 15 years from economically and socially backward rural areas of India and make them stand firmly on the track of literacy through quality primary education is. Project Nanhi Kali is the beginning of an initiative to fulfill our mission to develop our country.",
	"educational" => "Educational",
	"financial" => "Financial",
	"says" => "Our Voluteers Says",
	"working" => "Working in the field of responsible tourism gives me the opportunity to see many interesting projects worldwide. Though, would I have to pick my favourite project now, that would be the Project Nanhi Kali that helps the  poor girls by giving them opportunity to complete their education and fulfil their dreams. ",
	"nishant" => "Nishant",
	"founder" => "Business Founder",
	"enjoyed" => "Enjoyed the visit and glad it finally happened and I could see all the good work being done in person and to see so many bright sparks amongst the girl students, enthusiastic & confident to stand up and speak in front of new people.",
	"usha" => "Usha",
	"educate" => "�Educate Girls is the most cost-effective education intervention working at a significant scale we have found � and we are happy to partner with them.�",
	"sam" => "Sam Morgan",
	"fantastic" => "�Nanhi Kali is a fantastic NGO. It is already exceeding its targets in providing and supporting quality primary education for girls and addressing the critical issue of retention. We look forward to seeing Nanhi Kali grow and thrive in the coming years.�",
	"neera" => "Neera Nundy",
	"cofounder" => "Partner and Co-Founder, Dasra",
];
