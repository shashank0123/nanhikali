<?php 
return [
	"gallery" => "Image Gallery",
	"catch" => "Catch a glimpse of the candid and innocent moments in the lives of our Nanhi Kalis",
	"image" => "Images",
	"video" => "Video",
	"no" => "No Video Available Right Now.",
];