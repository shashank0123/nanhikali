<?php 
use App\Cause;	
use App\State;	
use App\City;	
use App\Address;	
$allcauses = Cause::where('status','Active')->where('cause_status','in_process')->get();
$states = State::all();
$user = Auth::user();
$address = '';

//echo $user;

if(!empty($user)){
	$address = Address::where('user_id',$user->id)->first();
//	echo $address;
}
//die;

?>



<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Basic Page Needs
    	================================================== -->
    	<meta charset="utf-8">
    	<!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<title>Nanhi Kali | Home</title>
    	<meta name="description" content="Nanhi Kali - ">
    	<meta name="keywords" content="campaign, cause, charity, donate, donations, event, foundation, fund, fundraising, kids, ngo, non-profit, organization, volunteer">
    	<meta name="author" content="rometheme.net"> 
    	<!-- CSRF Token -->
    	<meta name="csrf-token" content="{{ csrf_token() }}" />

	<!-- ==============================================
	Favicons
	=============================================== -->
	<link rel="shortcut icon" href="/logo/favicon.png">
	<link rel="apple-touch-icon" href="/assets/images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/assets/images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/assets/images/apple-touch-icon-114x114.png">
	
	<!-- ==============================================
	CSS VENDOR
	=============================================== -->
	<link rel="stylesheet" type="text/css" href="/assets/css/vendor/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="/assets/css/vendor/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/vendor/owl.carousel.min.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/vendor/owl.theme.default.min.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/vendor/magnific-popup.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/vendor/animate.min.css">
	
	<!-- ==============================================
	Custom Stylesheet
	=============================================== -->
	<link rel="stylesheet" type="text/css" href="/assets/css/style.css" />
	
	<script src="/assets/js/vendor/modernizr.min.js"></script>





	<style>
		#payment-mode{ display: none; }
		.sub-nav { width: auto; background-color: #fff; padding: 1% 5% 1% 1%;z-index: 999; position: absolute; display: none;}
		.sub-nav ul{ padding: 5% 2%; }
		.dropdown{ margin-top: -10px }
		.sub-nav li{ padding: 10% 0%;  }
		.dropdown-content {
			display: none;
			position: absolute;
			background-color: #f9f9f9;
			min-width: 160px;
			box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
			z-index: 1;
		}
		.dropdown-content a {
			color: black;
			padding: 12px 16px;
			text-decoration: none;
			display: block;
		}

		.dropdown-content a:hover {
			color: #9D6C34;
		}

		/* Show the dropdown menu on hover */
		.dropdown:hover .dropdown-content {
			display: block;
		}

		.form-payment{
			width: auto !important;  
			display: inline-block;
			height: auto !important; 
			border: 1px solid #ebeef0;
			background: #fafbfb;
			padding: 10 !important; 
			margin-top: 10px !important; 
			margin-bottom: 10px !important; 
		}

		#success-volunteer { 
			background-color: #ff0000; color: #fff; width: 100%; padding: 20px; text-align: center; display: none; margin-top: 10px}

			.hello a:hover{
				text-decoration: underline;
			}

			#d-type { display: none; }

			.goog-te-combo { border:none !important; top: 0;
    padding: 4px;
    width: 200px;
    border-radius: 3px;
    font-size: 11px !important;
    color: #000;  }

.goog-te-gadget { color: #fff !important; }
.goog-logo-link { color: #fff !important; }




#navigation{
border: none;
    height: 30px;
    width: 200px;
    margin-top: 9px;
    border-radius: 4px;}
		</style>

	</head>

	<body>

		<!-- LOAD PAGE -->
		<div class="animationload">
			<div class="loader"></div>
		</div>

		<!-- BACK TO TOP SECTION -->
		<a href="#0" class="cd-top cd-is-visible cd-fade-out">Top</a>

		<!-- HEADER -->
		<div class="header header-1">
			<!-- TOPBAR -->
			<div class="topbar">
				<div class="container">
					<div class="row">

						<div class="col-sm-6 col-md-6">
							<div class="sosmed-icon pull-left">
								<a href="https://www.facebook.com/kalinanhi/"><i class="fa fa-facebook"></i></a> 
								<a href="https://twitter.com/KaliNahni"><i class="fa fa-twitter"></i></a> 
								<a href="https://www.instagram.com/nanhi.kali/"><i class="fa fa-instagram"></i></a> 
								<a href="https://www.linkedin.com/in/nanhi-kali-092753192/"><i class="fa fa-linkedin"></i></a> 
								<a href="https://www.youtube.com/channel/UCzBa4aHzT4fP2viYZqKVD-A?disable_polymer=true"><i class="fa fa-youtube"></i></a> 
							</div>
						</div>

						<div class="col-sm-6 col-md-6">
							<div class="hello pull-right">	
								<div class="row">
									
									<div class="col-sm-6">
										@if(empty($user->id))							
										<li><a href="/login" style="color: #fff; font-size: 20px;">Login</a>&nbsp;&nbsp;&nbsp;</li>

										@else

										<li class="dropdown " >
											<a class="nav-link" href="#gallery"  style="color: #fff; font-size: 20px; padding">
												{{strtoupper($user->name)}}
											</a>
											<div class="dropdown-content">
												<a href="/my-profile">Profile</a>
												<a href="{{ route('logout') }}"
												onclick="event.preventDefault();
												document.getElementById('logout-form').submit();">
												Logout
											</a>

											<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
												@csrf
											</form>

										</div>
									</li>
									@endif
								</div>

								<div class="col-sm-6">
										<select id="navigation">
										    <option value="locale/en">English</option>
										    <option value="locale/hi">Hindi</option>
										</select>
									</div>

							</div>								


						</div>
					</div>

				</div>
			</div>
		</div>

		<!-- MIDDLE BAR -->
		<div class="middlebar">
			<div class="container">				

				<div class="contact-info">
					<!-- INFO 1 -->
					<div class="box-icon-1">
						<div class="icon">
							<div class="fa fa-envelope-o"></div>
						</div>
						<div class="body-content">
							<div class="heading">Mail :</div>
							<a href="mailto:info@nanhi-kali.org" subject="Hello">info@nanhi-kali.org</a>
						</div>
					</div>
					<!-- INFO 2 -->
					<div class="box-icon-1">
						<div class="icon">
							<div class="fa fa-phone"></div>
						</div>
						<div class="body-content">
							<div class="heading">Call Us :</div>
							<a href="tel:+91 97287 73730">+91 97287 73730</a>/<a href="tel:+91-9354149738">+91-9354149738</a> 

						</div>
					</div>
					<!-- INFO 3 -->
					<div class="box-act">
						<a class="btn btn-lg btn-primary" data-toggle="modal" data-target="#exampleModalLong">DONATE NOW</a>
					</div>					
				</div>
			</div>
		</div>

		<!-- NAVBAR SECTION -->
		<div class="navbar-main">
			<div class="container">
				<nav class="navbar navbar-expand-lg">
					<a class="navbar-brand" href="/">
						<img src="/logo/nanhi_kali_logi.png" alt="" style="width: auto; height: 60px;" />
					</a>

					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarNavDropdown">
						<ul class="navbar-nav">
							<li class="nav-item ">
								<a class="nav-link" href="/">
									HOME
								</a>

							</li>
							<li class="nav-item ">
								<a class="nav-link" href="/about-us">
									ABOUT US
								</a>

							</li>
							<li class="nav-item ">
								<a class="nav-link" href="/cause">
									CAUSES
								</a>

							</li>

							<li class="nav-item ">
								<a class="nav-link" href="/events">
									EVENTS
								</a>

							</li>
							<li class="nav-item ">
								<a class="nav-link" href="/blogs">
									BLOG
								</a>

							</li>

							<li class="dropdown " onmouseover="showGallery()">
								<a class="nav-link" href="#gallery">
									GALLERY
								</a>
								<div class="dropdown-content">
									<a href="/gallery">Images</a>
									<a href="/videos">Videos</a>

								</div>
							</li>
							<li class="nav-item ">
								<a class="nav-link" href="/awards&achievements">
									AWARDS & ACHIEVEMENTS
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="/faq">FAQ</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="/our-team">OUR TEAM</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="/contact-us">CONTACT US</a>
							</li>
						</ul>
					</div>
				</nav> <!-- -->
			</div>
		</div>
	</div>

	@yield('content')

	<!-- CTA -->
	<div class="section cta">
		<div class="content-wrap-20">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<div class="cta-1">
							<div class="body-text">
								<h3>Join your hand with us for a better life and bright future.</h3>
							</div>
							<div class="body-action">
								<a  data-toggle="modal" data-target="#exampleModalLong" class="btn btn-secondary">DONATE NOW</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- FOOTER SECTION -->
	{{-- <div class="footer" data-background="/assets/images/dummy-img-1920x900-3.jpg"> --}}
		<div class="footer" style="background-color: #000" >
			<div class="content-wrap">
				<div class="container">

					<div class="row">
						<div class="col-sm-3 col-md-3">
							<div class="footer-item">
								<img src="/logo/nanhi_kali_logi.png" alt="logo bottom" class="logo-bottom">
								<div class="spacer-30"></div>
								<p>Project Nanhi Kali is an Indian Non-Governmental Organization that supports education for under-privileged girls in India.</p>
								<a href="/about-us"><i class="fa fa-angle-right"></i> Read More</a>
							</div>
						</div>

						<div class="col-sm-3 col-md-3">
							<div class="footer-item">
								<div class="footer-title">
									WHO WE ARE
								</div>

								<div class="row">
									<div class="col-sm-6 col-md-6">
										<ul class="list">
											<li><a href="/about-us" title="About us">About us</a></li>
											<li><a href="/cause" title="Couses">Causes</a></li>
											<li><a href="/testimonials" title="Testimonials">Testimonials</a></li>
											<li><a href="/gallery" title="Gallery">Gallery</a></li>
											<li><a href="/faq" title="Faq">Faq</a></li>
										</ul>
									</div>
									<div class="col-sm-6 col-md-6">
										<ul class="list">
											<li><a href="/our-team" title="Our Team">Our Team</a></li>
											<li><a href="/events" title="Events">Events</a></li>
											<li><a href="/contact-us" title="Contact Us">Contact Us</a></li>
											<li><a href="/privacy-policy" title="Privacy Policy">Privacy Policy</a></li>
											<li><a href="/disclaimer" title="Disclaimer">Disclaimer</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>

						<div class="col-sm-3 col-md-3">
							<div class="footer-item">
								<div class="footer-title">
									WHERE WE WORK
								</div>
								<ul class="list-info">
									<li>
										<div class="info-icon">
											<span class="fa fa-map-marker"></span>
										</div>
										<div class="info-text">Experion heartsong<br>
											Sector 108<br>
											A1/506<br>
											Dwarka Express Way<br>
										Gurugram.. 122017 </div> </li>
										<li>
											<div class="info-icon">
												<span class="fa fa-phone"></span>
											</div>
											<div class="info-text">
												<a href="tel:+91-9728773730">+91-9728773730</a>
												<br>
												<a href="tel:+91-9354149738">+91-9354149738</a><br>

												<a href="tel:+91-9591804800">+91-9591804800</a>
											</div>
										</li>
										<li>
											<div class="info-icon">
												<span class="fa fa-envelope"></span>
											</div>
											<div class="info-text"><a href="mailto:info@nanhi-kali.org" subject="Hello">info@nanhi-kali.org</a></div>
										</li>
									</ul>
								</div>
							</div>

							<div class="col-sm-3 col-md-3">
								<div class="footer-item">
									<div class="footer-title">
										FOLLOW US
									</div>
									<p>Follow us on</p>
									<div class="sosmed-icon primary">
										<a href="https://www.facebook.com/kalinanhi/"><i class="fa fa-facebook"></i></a> 
										<a href="https://twitter.com/KaliNahni"><i class="fa fa-twitter"></i></a> 
										<a href="https://www.instagram.com/nanhi.kali/"><i class="fa fa-instagram"></i></a> 
										<a href="https://www.linkedin.com/in/nanhi-kali-092753192/"><i class="fa fa-linkedin"></i></a> 
										<a href="https://www.youtube.com/channel/UCzBa4aHzT4fP2viYZqKVD-A?disable_polymer=true"><i class="fa fa-youtube"></i></a> 
									</div>
								</div>
							</div>
						</div><br>
						<form method="POST" action="" id="newsletter-form">
							<div class="row" class="newsletter-foot" style="">
								@csrf
								<div class="col-sm-6 col-12">
									<h4><strong style="color: #9D6C34">Subscribe</strong> Here For Newsletter</h4> &nbsp;&nbsp;&nbsp;
								</div>
								<div class="col-sm-4 col-8">			

									<input type="email" class="form-control" name="email" style="width: 100%; right: 0; padding: 23px !important" placeholder="Enter your Email" />
								</div>
								<div class="col-sm-2 col-4" style="text-align: left;">
									<button type="button" name="submit" value="Submit" class="btn btnsbmt btn-secondary" onclick="submitNewsletter()">Submit</button>
								</div>

								<div class="col-sm-12" style="text-align: center;">
									<p id="news-response" style="color : #fff; font-size: 24px"></p>
								</div>
							</div>
						</form>
					</div>
				</div>

				<div class="fcopy">
					<div class="container">
						<div class="row">
							<div class="col-sm-12 col-md-12">
								<p class="ftex">Copyright 2019 &copy; <span class="color-primary"></span>. Designed by <span class="color-primary"><a href="http://www.backstagesupporters.com">Backstage Supporters.</a></span></p> 
							</div>
						</div>
					</div>
				</div>

			</div>

			<style type="text/css">

			</style>

			<!-- JS VENDOR -->
			<script src="/assets/js/vendor/jquery.min.js"></script>
			<script src="/assets/js/vendor/bootstrap.min.js"></script>
			<script src="/assets/js/vendor/owl.carousel.js"></script>
			<script src="/assets/js/vendor/jquery.magnific-popup.min.js"></script>

			<!-- SENDMAIL -->
			<script src="/assets/js/vendor/validator.min.js"></script>
			<script src="/assets/js/vendor/form-scripts.js"></script>

			<script src="/assets/js/script.js"></script>
			
			@yield('script')
			<!--=========model=================-->
			<!-- Button trigger modal -->

			<!-- Modal -->
			<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							{{-- <h5 class="modal-title" id="exampleModalLongTitle">Donate Now</h5> --}}
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<!--======= MAKE DONATION POPUP START =========-->
							<div id="make-donation" class="donation-pop zoom-anim-dialog">

								<form method="POST" action="" id="donation-form">
									@csrf
									<!--======= MAKE DONATION POPUP START =========-->
									<h6>Make a Donation</h6>
									<!--======= DONATE LIGHT BOX =========-->
									<div class="pop-inner">
										<label>Project - You Want To donate for</label>
										<select type="text" name="project_slug" class="form-control" id="project_slug">
											<option>- Select Project -</option>			
											@if(!empty($allcauses))
											@foreach($allcauses as $cause)
											<option value="{{$cause->slug}}">{{$cause->title}}</option>
											@endforeach
											@endif
										</select>
										<br>

										<label> Type of Donar</label>
										<select type="text" name="donar_type" class="form-control" id="donar_type">
											
											<option>- Select Donar Type -</option>
											<option value="individual">Individual</option>
											<option value="corporate">Corporate</option>
											
										</select>
										<br>

										<label>Donaiton Type</label>
										<select type="text" name="donation_type" class="form-control" id="donation_type" onchange="getInstall()">
											
											<option>- Select Donation Type -</option>
											<option value="one_time">One Time</option>
											<option value="emi">EMI</option>
											
										</select>
										<br>

										<label>Donation Amount</label>
										<input type="text" name="donated_amount" class="form-control" placeholder="Enter Donation Amount Here" />
										<br>

										<div id="d-type">
											<label>Installment (Per Month)</label>
											<input type="text" name="installment" class="form-control" id="installment" placeholder="Installment (Per Month)">
											
											<br>
										</div>
										
										<ul class="row per-info">
											<li class="col-lg-12">
												<label>Your Information</label>
											</li>

											<li class="col-sm-12">
												<input type="text" id="vendor_name" name="vendor_name" placeholder="Full Name"  class="form-control" required value="@if(!empty($user)) {{$user->name}} @endif" @if(!empty($user)) {{'readonly'}} @endif>
											</li>

											<li class="col-sm-12">
												<input type="email" id="email" name="email" placeholder="Email"  class="form-control" onchange="validateEmail()" required value="@if(!empty($user)) {{$user->email}} @endif" @if(!empty($user)) {{'readonly'}} @endif>
												<p id="validate-email"></p>
											</li>
											<li class="col-sm-12">
												<input type="phone" id="phone" name="phone" placeholder="Phone"  class="form-control" onchange="validatePhone()" required value="@if(!empty($user)) {{$user->phone}} @endif" @if(!empty($user)) {{'readonly'}} @endif>
												<p id="validate"> </p>
											</li>
										</ul>	
										
										@if(!empty($address))
										<style>
											#hide-address{ display: none; }
										</style>
										@endif

										<ul class="row per-info" id="hide-address">
											<li class="col-lg-12">
												<label>Your Address</label>
											</li>

											<li class="col-sm-12">
												<input type="text" id="address" name="address" placeholder="Full Address"  class="form-control" required value="@if(!empty($address)) {{$address->address}} @endif">
											</li>

											<li class="col-sm-12">
												<select id="state" name="state" class="form-control" readonly>
													<option>- Select State -</option>
													@foreach($states as $state)
													<option value="{{$state->id}}" @if(!empty($address->state)) @if($address->state == $state->id) {{'selected'}}  @endif @endif>{{$state->state}}</option>
													@endforeach
												</select>
												
											</li>
											<li class="col-sm-12">
												<select id="city" name="city" class="form-control" readonly>
													@if(!empty($address->city))
													<?php
													$city = City::where('id',$address->city)->first();
													echo "<option value='".$city->id."'>".$city->city."</option>";
													?>

													@else
													<option>- Select City -</option>
													@endif
												</select>
											</li>

											<li class="col-sm-12">
												<input type="text" id="postel_code" name="postel_code"  class="form-control" placeholder="Postal Code" required value="@if(!empty($address)) {{$address->pin_code}} @endif">
											</li>
										</ul>
									</div>
									<div style="text-align: center;">
										<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										<span onclick="selectMode()" class="btn btn-primary">Continue</span>
									</div>

								</form>

								{{-- <form method="POST" action="" id="payment-mode"> --}}
									<form method="POST" action="" id="payment-mode">
										@csrf
										<h6>Make a Donation</h6>
										<!--======= DONATE LIGHT BOX =========-->
										<div class="pop-inner">									
											<input type="hidden" value="" id="emailvalue" name="emailvalue">
											<input type="hidden" value="" id="phonevalue" name="phonevalue">
											<input type="hidden" value="" id="projectslug" name="projectslug">
											<ul class="row per-info">
												<li class="col-lg-12">
													<label>Select Mode</label>
												</li>

												<li class="col-sm-12">
													<input type="radio" class="form-payment" name="payment_mode" value="payumoney" placeholder="">PayUMoney
												</li>

												<li class="col-sm-12">
													<input type="radio" class="form-payment" name="payment_mode" placeholder="">GPay
												</li>
												<li class="col-sm-12">
													<input type="radio" class="form-payment" name="payment_mode" placeholder="">PayTM
												</li>
											</ul>										

										</div>
										<div style="text-align: center;">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											<span onclick="paymentPage()" class="btn btn-primary">Continue</span>
										</div>

									</form>
								</div>

							</div>
							<!--======= MAKE DONATION POPUP END =========--> 
						</div>

					</div>
				</div>
			</div>


			<!-- Modal -->
			<div class="modal fade" id="joinModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							{{-- <h5 class="modal-title" id="exampleModalLongTitle">Donate Now</h5> --}}
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<!--======= MAKE DONATION POPUP START =========-->
							<div id="make-donation" class="donation-pop zoom-anim-dialog">

								<form method="POST" action="" id="volunteer-form">
									@csrf
									<!--======= MAKE DONATION POPUP START =========-->
									<h6>Become a Volunteer</h6>
									<!--======= DONATE LIGHT BOX =========-->
									<div class="pop-inner">


										<label>Volunteer Name</label>
										<input type="text" name="volunteer_name" class="form-control" placeholder="Enter Name Here" required/>

										<label>Email</label>
										<input type="email" name="email" id="vemail" class="form-control" placeholder="Enter Email Here" onchange="validateEmail1()" required/>
										<p id="validate-vemail"></p>

										<label>Phone</label>
										<input type="phone" name="phone" id="mobile" class="form-control" placeholder="Enter Phone Number" onchange="validatePhone1()" required/>
										<p id="validate-phone"></p>

										<label>Message</label>
										<textarea type="text" name="message" class="form-control" placeholder="Enter Message" required/></textarea>

									</div>
									<div id="success-volunteer"></div><br>
									<div style="text-align: center;">
										<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										<button type="button" class="btn btn-primary" onclick="submitVolunteer()">Submit</button>
									</div>

								</form>


							</div>

						</div>
						<!--======= MAKE DONATION POPUP END =========--> 
					</div>

				</div>
			</div>
		</div>


		<script>

			

			function selectMode(){						
				$.ajax({							
					url: '/add-info',
					type: 'POST',							
					data: $('#donation-form').serialize(),
					success: function (data) { 
						if(data.message == 'Inserted Successfully'){
							alert('Done');
							$('#donation-form').hide();
							$('#payment-mode').show();
							$('#emailvalue').val($('#email').val())
							$('#phonevalue').val($('#phone').val())
							$('#projectslug').val($('#project_slug').val())
						}          
						else{
							alert('Fail');
						}
					},
					failure:function(data){
						alert(data.message);
					}
				}); 
			}

			function submitNewsletter(){						
				$.ajax({							
					url: '/submit-newsletter',
					type: 'POST',							
					data: $('#newsletter-form').serialize(),
					success: function (data) { 
						if(data.message == 'Thank you. We will respond you shortly.'){	
							$('#news-response').html(data.message);			
						}          
						else{
							alert(data.message);
						}
					},
					failure:function(data){
						alert(data.message);
					}
				}); 
			}

			function submitVolunteer(){						
				$.ajax({							
					url: '/submit-volunteer',
					type: 'POST',							
					data: $('#volunteer-form').serialize(),
					success: function (data) { 
						if(data.message == 'Welcome, We will respond you shortly.'){	
							$('#success-volunteer').show();			
							$('#success-volunteer').html(data.message);			
						}          
						else{
							alert(data.message);
						}
					},
					failure:function(data){
						alert(data.message);
					}
				}); 
			}

			function paymentPage(){

				$.ajax({
					/* the route pointing to the post function */
					url: '/add-payment-mode',
					type: 'POST',
					/* send the csrf-token and the input to the controller */
					data: $('#payment-mode').serialize(),
					success: function (data) { 
						if(data.message == 'Updated Successfully'){

							window.location.href = '/payment/'+data.id; 
						}          
						else{
							alert('Fail');
						}
					},
					failure:function(data){
						alert(data.message);
					}
				});
			}

			function validatePhone(){
				var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
				var phone = $('#phone').val();
				$.ajax({							
					url: '/validate-phone',
					type: 'POST',							
					data: {_token: CSRF_TOKEN, phone: phone},
					success: function (data) { 
						if(data.success == 'false'){
							if(data.message.phone){
								$('#validate').show();
								$('#validate').html(data.message.phone[0]);	 }	
							}       
							else{
								$('#validate').hide();	}									
							},
							failure:function(data){
								$('#validate').html(data.message);
							}
						});
			}

			function validatePhone1(){
				var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
				var phone = $('#mobile').val();
				$.ajax({							
					url: '/validate-phone',
					type: 'POST',							
					data: {_token: CSRF_TOKEN, phone: phone},
					success: function (data) { 
						if(data.success == 'false'){
							if(data.message.phone){	
								$('#validate-phone').show();									
								$('#validate-phone').html(data.message.phone[0]);	}	
							}       
							else{									
								$('#validate-phone').hide(); }								
							},
							failure:function(data){
								$('#validate-phone').html(data.message);
							}
						});
			}

			function validateEmail(){
				var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
				var email = $('#email').val();
				$.ajax({							
					url: '/validate-email',
					type: 'POST',							
					data: {_token: CSRF_TOKEN, email: email},
					success: function (data) { 
						if(data.success == 'false'){
							if(data.message.email){
								$('#validate-email').show();
								$('#validate-email').html(data.message.email[0]);	 }	
							}       
							else{
								$('#validate-email').hide();	}							
							},
							failure:function(data){
								$('#validate-email').html(data.message);
							}
						});
			}

			function validateEmail1(){
				var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
				var email = $('#vemail').val();
				$.ajax({							
					url: '/validate-email',
					type: 'POST',							
					data: {_token: CSRF_TOKEN, email: email},
					success: function (data) { 
						if(data.success == 'false'){
							if(data.message.email){		
								$('#validate-vemail').show();								
								$('#validate-vemail').html(data.message.email[0]);	}	
							}       
							else{									
								$('#validate-vemail').hide(); }								
							},
							failure:function(data){
								$('#validate-vemail').html(data.message);
							}
						});
			}


			$('#state').on('change',function(){
				var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
				var state = $('#state').val();

				$.ajax({
					type: 'POST',
					url: '/cities',
					data: {_token: CSRF_TOKEN, state_id : state },
					success:function(data){
						$('#city').html(data);
					}
				});
			});

			function getInstall(){
				var type = $('#donation_type').val();

				if(type == 'emi'){
					$('#d-type').show();
				}
				else{
					$('#d-type').hide();
				}
			}

			$("#navigation").change(function()
			{
			    document.location.href = $(this).val();
			});




		</script>

		<!-- end model -->
	</body>
	</html>