<?php 
use App\City;
use App\State;
use App\Event;
use App\Donation;
?>

@extends('layouts/front')


@section('content')

<!-- BANNER -->
<div id="oc-fullslider" class="banner owl-carousel">
	@if(!empty($banners))
	@foreach($banners as $banner)
	<div class="owl-slide">
		<div class="item">
			<img src="/banner-images/{{$banner->image_url}}" alt="Slider">
			<div class="slider-pos">
		            {{-- <div class="container">
		            	<div class="wrap-caption">
			                <h1 class="caption-heading bg"><span>#EndViolence</span> For Every Woman</h1>
			                <p class="bg">Remipsum dolor sit amet consectetur adipisicing</p>
			                <a href="#" class="btn btn-primary">DONATE NOW</a>
			            </div>  
			        </div> --}}
			    </div>
			</div>
		</div>
		@endforeach
		@endif

	</div>

	<div class="clearfix"></div>

	<!-- WE NEED YOUR HELP -->
	<div class="section services">
		<div class="content-wrap1">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<h2 class="section-heading center">
							{{ __('home.we') }} <span>{{ __('home.need') }}</span> {{ __('home.your_help') }}
						</h2>
						<p class="subheading text-center" style="color: #999999">{{ __('home.please') }}</p>
					</div>
					<div class="clearfix"></div>
					<!-- Item 1 -->
					@if(!empty($top_causes))
					@foreach($top_causes as $top)
					<?php 
					$donate = Donation::where('project_slug',$top->slug)->where('status','success')->get();
					$donate_amt = 0;
					$donation_remain = 0;

					if(!empty($donate)){
						foreach($donate as $done){
							$donate_amt = $donate_amt + $done->donated_amount;
						}
					}

					$donation_remain = $top->targeted_amount - $donate_amt;
					$percent = ($donate_amt*100)/$top->targeted_amount;
					?>
					<div class="col-sm-4 col-md-4">
						<div class="box-fundraising">
							<div class="media">
								<img src="/causes-img/{{$top->image_url}}" alt="" style="width: 2000px; height: 300px">
							</div>
							<div class="body-content">
								<p class="title"><a href="/single-cause/{{$top->slug}}">{{strtoupper($top->title)}}</a></p>
								<div class="text">{{substr($top->short_description,0,100)}}..</div>
								<div class="progress-fundraising">
									<div class="total">Rs. {{$donation_remain}} <span>to go</span></div>
									<div class="persen">{{$percent}}%</div>
									<div class="progress">
										<div class="progress-bar" role="progressbar" aria-valuenow="{{$percent}}" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					@endforeach
					@endif

					<div class="col-sm-12 col-md-12">
						<div class="spacer-50"></div>
						<div class="text-center">
							<a href="/cause" class="btn btn-primary">{{ __('home.cause') }}</a>
						</div>
						<div class="spacer-50"></div>

					</div>

				</div>
			</div>
		</div>
	</div>

	<!-- URGENT CAUSE -->
	@if(!empty($urgent_cause))
	<?php 
	$donate = Donation::where('project_slug',$top->slug)->where('status','success')->get();
	$donate_amt = 0;
	$donation_remain = 0;

	if(!empty($donate)){
		foreach($donate as $done){
			$donate_amt = $donate_amt + $done->donated_amount;
		}
	}

	$donation_remain = $top->targeted_amount - $donate_amt;
	$percent = ($donate_amt*100)/$top->targeted_amount;
	?>
	<div class="section" data-background="/banner-images/NGO-7.png">
		<div class="content-wrap" style="z-index: 999">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-md-6">
						<h2 class="section-heading light">
							Urgent <span>Cause</span>
						</h2>
						
						<h2 class="color-white"><span class="color-primary">#{{$urgent_cause->title}}</span></h2>
						<p style="color: #fff">{{substr($urgent_cause->short_description,0,100)}}..</p>

						<div class="progress-fundraising progress-lg">
							<div class="total">Donated</div>
							<div class="persen color-white">{{$percent}}%</div>
							<div class="progress">
								<div class="progress-bar" role="progressbar" aria-valuenow="{{$percent}}" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
							<div class="detail">
								<h3>Rs {{$donation_remain}} <small class="color-white">to go.</small> <small class="color-white"></small></h3>
							</div>
						</div>

					</div>

					<div class="col-sm-6 col-md-6">
						<div class="img-button">
							<img src="/causes-img/{{$urgent_cause->image_url}}" alt="" style="height: 350px; width: auto">
							<div class="btn-overlay">
								
								<a class="btn btn-lg btn-primary" data-toggle="modal" data-target="#exampleModalLong">DONATE NOW</a>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
	@endif

	<!-- HOW TO HELP US -->
	<div class="section">
		<div class="content-wrap-top">
			<div class="container">
				<div class="row align-items-end">

					<div class="col-sm-6 col-md-6">
						<img src="/banner-images/NGO-5.png" alt="" class="img-fluid" style="height: 700px !important">
					</div>

					<div class="col-sm-6 col-md-6">
						<h2 class="section-heading">
							How To <span>Help</span> Us
						</h2>
						<div class="section-subheading">Project Nanhi Kali is a participatory project where you can sponsor the education of an underprivileged girl child. You can sponsor a Nanhi Kali from primary school studying in class 1-5 at just Rs. 3600 a year, while for Rs. 4800 you can sponsor a Nanhi Kali from secondary school studying in class 6-10. Thereafter, in the first year, you will receive the photograph, profile and progress report of the Nanhi Kali your support, so you are updated on how she is faring in both academics as well as extracurricular activities.</div> 
						<div class="margin-bottom-50"></div>
						<dl class="hiw">
							<dt><span class="fa fa-gift"></span></dt>
							<dd><div class="no">01</div><h3>Send Donation</h3>Please come forward and contribute. Your contribution will transform lives of many.</dd>
							<dt><span class="fa fa-male"></span></dt>
							<dd><div class="no">02</div><h3>Become Volunteer</h3>You can sponsor the education of one or more underprivileged girls. You will have the option to choose the number and education level of the girls you are sponsoring.</dd>
							<dt><span class="fa fa-bullhorn"></span></dt>
							<dd><div class="no">03</div><h3>Share Media</h3>Increase our followers on Twitter, Expand our network on Facebook, Write stories and updates for our Blog & Follow us on Instagram.
							</dd>
							
						</dl>
						<div class="spacer-70"></div>
					</div>

				</div>
			</div>
		</div>
	</div>

	<!-- WE HELP MANY PEOPLE -->
	<div class="section" data-background="/banner-images/NGO-8.png">
		<div class="content-wrap">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<div class="cta-info color-white">
							<h1 class="section-heading light no-after">
								<span>We Help Many</span> People
							</h1>
							<h3 class="color-primary">Want to Become a Volunteer</h3>

							<div class="spacer-10"></div>
							<p>Volunteers do not necessarily have the time; they just have the heart. You can sponsor the education of one or more underprivileged girls. You will have the option to choose the number and education level of the girls you are sponsoring.</p>

							<a href="/about-us" class="btn btn-light btnlrn margin-btn">LEARN MORE</a> <a  data-toggle="modal" data-target="#joinModel" class="btn btn-primary margin-btn">JOIN US NOW</a>	
						</div>

					</div>

				</div>
			</div>
		</div>
	</div>

	<!-- OUR VOLUUNTER SAYS -->
	<div class="section">
		<div class="content-wrap">
			<div class="container">
				<div class="row">

					<div class="col-sm-12 col-md-12">
						<h2 class="section-heading center">
							Our <span>Volunteers</span> Says
						</h2>
						
					</div>
					@if(!empty($testimonials))
					@foreach($testimonials as $test)
					<div class="col-sm-6 col-md-6">
						<div class="testimonial-1">
							<div class="media" style="text-align: center;">
								<img src="/testimonials-img/{{$test->image_url}}" alt="{{$test->title}}" class="img-fluid">
							</div>
							<div class="body">
								<p style="text-align: justify;"> <?php echo $test->review; ?></p>
								<div class="title">{{$test->name}}</div>
								<div class="company">{{$test->occupasion}}</div>
							</div>
						</div>
					</div>
					@endforeach
					@endif					

				</div>
			</div>
		</div>
	</div>

	<!-- OUR GALLERY -->
	<div class="section" data-background="/assets/images/dummy-img-1920x900-2.jpg">
		<div class="content-wrap">
			<div class="container">
				<div class="row">

					<div class="col-sm-12 col-md-12">
						<h2 class="section-heading center">
							Our <span>Gallery</span>
						</h2>
						<p class="subheading text-center">Catch a glimpse of the candid and innocent moments in the lives of our Nanhi Kalis.</p>
					</div>

					<div class="row popup-gallery gutter-5">

						@if(!empty($galleries))
						@foreach($galleries as $gallery)
						<div class="col-xs-12 col-md-4">
							<div class="box-gallery">
								<a href="/banner-images/{{$gallery->image_url}}" title="{{$gallery->image_text}}">
									<img src="/banner-images/{{$gallery->image_url}}" alt="" class="img-fluid"  style="width: 100%; height: 300px;">
									<div class="project-info">
										<div class="project-icon">
											<span class="fa fa-search"></span>
										</div>
									</div>
								</a>
							</div>
						</div>
						@endforeach
						@endif
						
					</div>

				</div>
			</div>
		</div>
	</div>

	<!-- EVENTS -->
	<div class="section" data-background="/banner-images/NGO-7.png">
		<div class="content-wrap">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-md-6">
						<?php

						$events = Event::where('status','present')->orderBy('created_at','DESC')->limit(1)->first();
						?>
						@if(!empty($events))
						<div class="event-pin">
							<h1 class="color-white">#Events</h1>
							<h3><a href="/single-event/{{$events->slug}}">{{$events->title}}</a></h3>
							<p>{{substr($events->short_description,0,100)}}..</p>
							<a href="/single-event/{{$events->slug}}" class="read-more">Read More...</a>
							<div class="more-event"><a href="#"><i class="fa fa-long-arrow-left"></i></a></div>
						</div>
						@endif

					</div>

					<div class="col-sm-6 col-md-5 col-md-offset-1">
						<h2 class="section-heading light">
							<span>Upcoming</span> Events
						</h2>
						<p style="color: #fff">Give your precious time to our Nanhi Kalis by becoming part of our upcoming events.</p>

						<div class="list-events">
							@if(!empty($upcoming_events))
							@foreach($upcoming_events as $event)
							<div class="box-event">
								<div class="meta-date">
									<div class="date">{{$event->date}}</div>
									{{-- <div class="month">AUG</div> --}}
								</div>
								<div class="body-content">
									<h4><a href="/single-event/{{$event->slug}}">{{$event->title}}</a></h4>
									<div class="meta" style="color: #9D6C34">
										<span class="date"><i class="fa fa-clock-o"></i>  {{$event->timing}}</span>
										<?php
										$city = City::where('id',$event->city)->first();
										$state = State::where('id',$event->state)->first();
										?>
										<span class="location"><i class="fa fa-map-marker"></i> {{$city->city}}, {{$state->state}}</span>
									</div>
									<p style="color: #fff">{{substr($event->short_description,0,100)}}..</p>
								</div>
							</div>
							@endforeach
							@endif
							
						</div>
						
					</div>

				</div>


			</div>
		</div>
	</div>

	<!-- OUR PARTNERS -->
	<div class="section">
		<div class="content-wrap">
			<div class="container">
				<div class="row">

					<div class="col-sm-12 col-md-12">
						<h2 class="section-heading center">
							Our <span>Partners</span>
						</h2>
						<p class="subheading text-center">To transform the lives of people in India through education, by providing financial assistance and recognition to them, across age groups and income strata.</p>
					</div>
					
				</div>
				<div class="row gutter-5">
					@if(!empty($partners))
					@foreach($partners as $partner)
					<div class="col-6 col-md-2">
						<a href="#"><img src="/banner-images/{{$partner->image_url}}" alt="{{$partner->image_text}}" class="img-fluid img-border"></a>
					</div>
					@endforeach
					@endif
					
				</div>
			</div>
		</div>
	</div>

	@endsection