@extends('layouts.front')

@section('content')


<!-- BANNER -->
<div class="section banner-page" data-background="/banner-images/NGO-6.png">
	<div class="content-wrap pos-relative">
		<div class="d-flex justify-content-center bd-highlight mb-3">
			<div class="title-page">Video
				<br>
				<p style="font-size: 14px">Catch a glimpse of the candid and innocent moments in the lives of our Nanhi Kalis.</p>
			</div>
		</div>
		<div class="d-flex justify-content-center bd-highlight mb-3">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb ">
					<li class="breadcrumb-item"><a href="/">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">Videos</li>
				</ol>
			</nav>
		</div>
	</div>
</div>


<!-- OUR GALLERY -->
<div class="section">
	<div class="content-wrap">
		<div class="container">

			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="row popup-gallery gutter-5">
						<?php
						$i=0;
						?>
						@if(!empty($videos))

						@foreach($videos as $video)
						<?php $i++; ?>
						<!-- ITEM 1 -->
						<div class="col-xs-6 col-md-3">
							<div class="img-video">
								<a href="{{$video->video_url}}" class="popup-youtube play-video"><i class="fa fa-play fa-2x"></i></a>
								<img src="/banner-images/NGO-8.png" alt="" style="width: 600pc; height: 400px">
								<div class="ripple"></div>
							</div>
						</div>
						@endforeach
						@endif

						@if($i==0)
						<div class="col-sm-12" style="text-align: center;">
							<h3 style="padding: 5%"> No Video Available Right Now.</h3>
						</div>
						@endif							

					</div>
				</div>

			</div>

		</div>
	</div>
</div>


@endsection
