<?php
use App\City;
use App\State;	
?>

@extends('layouts.front')

@section('content')

<!-- BANNER -->
	<div class="section banner-page" data-background="/banner-images/NGO-6.png">
		<div class="content-wrap pos-relative">
			<div class="d-flex justify-content-center bd-highlight mb-3">
				<div class="title-page">Events</div>
			</div>
			<div class="d-flex justify-content-center bd-highlight mb-3">
			    <nav aria-label="breadcrumb">
				  <ol class="breadcrumb ">
				    <li class="breadcrumb-item"><a href="/">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Events</li>
				  </ol>
				</nav>
		  	</div>
		</div>
	</div>

	<!-- HOW TO HELP US -->
	<div class="section">
		<div class="content-wrap">
			<div class="container">

				<div class="row">

					@if(!empty($events))
					@foreach($events as $event)
					<!-- Item 1 -->
					<div class="col-sm-4 col-md-4">
			            <div class="box-fundraising">
		              		<div class="media">
		              			<div class="meta-date">
									<div class="date">{{$event->date}}</div>
									{{-- <div class="month">AUG</div> --}}
								</div>
		                		<img src="/events-img/{{$event->image_url}}" alt="" style="width: 100%; height: 250px">
		              		</div>
		              		<div class="body-content">
		              			<p class="title"><a href="/single-event/{{$event->slug}}">{{strtoupper($event->title)}}</a></p>
		              			<div class="meta">
									<span class="date"><i class="fa fa-clock-o"></i>  {{$event->timing}}</span>
									<?php
									$city = City::where('id',$event->city)->first();
									$state = State::where('id',$event->state)->first();
									?>
									<span class="location"><i class="fa fa-map-marker"></i> {{$city->city}}, {{$state->state}}</span>
								</div>
		              			<div class="text">{{substr($event->short_description,0,100)}}..</div>
		              			<div class="spacer-30"></div>
		              			<a href="/single-event/{{$event->slug}}" class="btn btn-primary">READ MORE</a>
		              		</div>
			            </div>
			        </div>
			        @endforeach
			        @endif
			        

				</div>

			</div>
		</div>
	</div>
	
	


@endsection