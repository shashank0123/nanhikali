@extends('layouts.front')

@section('content')

<!-- BANNER -->
	<div class="section banner-page" data-background="/banner-images/NGO-6.png">
		<div class="content-wrap pos-relative">
			<div class="d-flex justify-content-center bd-highlight mb-3">
				<div class="title-page">About Us</div>
			</div>
			<div class="d-flex justify-content-center bd-highlight mb-3">
			    <nav aria-label="breadcrumb">
				  <ol class="breadcrumb ">
				    <li class="breadcrumb-item"><a href="/">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">About Us</li>
				  </ol>
				</nav>
		  	</div>
		</div>
	</div>

	<!-- WHY CHOOSE US -->
	<div class="section">
		<div class="content-wrap">
			<div class="container">
					<h3 style="text-align: center;">{{$content->page_name}}</h3>
				<div class="row">
					<div class="col-sm-1"></div>
					<div class="col-sm-10" style="text-align: justify;">
						<?php
						echo $content->content;
						?>
					</div>
					<div class="col-sm-1"></div>

					

				</div>
			</div>
		</div>
	</div>
	
	<!-- OUR VOLUUNTER SAYS -->
	<div class="section" data-background="/banner-images/NGO-7.png">
		<div class="content-wrap">
			<div class="container">
				<div class="row">

					<div class="col-sm-12 col-md-12">
						<h2 class="section-heading center" style="color: #fff;">
							Our <span style="color: #9D6C34">Volunteers</span> Says
						</h2>
						{{-- <p class="subheading text-center">Lorem ipsum dolor sit amet, onsectetur adipiscing cons ectetur nulla. Sed at ullamcorper risus.</p> --}}
					</div>

					@if(!empty($testimonials))
					@foreach($testimonials as $test)
					<div class="col-sm-6 col-md-6">
						<div class="testimonial-1">
		              		<div class="media" style="text-align: center;">
	              				<img src="/testimonials-img/{{$test->image_url}}" alt="" class="img-fluid">
	              			</div>
			              	<div class="body">
			                	<p style="color: #fff !important"><?php echo $test->review; ?> </p>
			                	<div class="title" style="color: #9D6C34">{{$test->name}}</div>
			                	<div class="company" style="color: #fff">{{$test->occupasion}}</div>
			              	</div>
			            </div>
					</div>
					@endforeach
					@endif
					
				</div>
			</div>
		</div>
	</div>

	<!-- OUR PARTNERS -->
	<div class="section">
		<div class="content-wrap">
			<div class="container">
				<div class="row">

					<div class="col-sm-12 col-md-12">
						<h2 class="section-heading center">
							Our <span>Partners</span>
						</h2>
						{{-- <p class="subheading text-center">Lorem ipsum dolor sit amet, onsectetur adipiscing cons ectetur nulla. Sed at ullamcorper risus.</p> --}}
					</div>
					
				</div>
				<div class="row gutter-5">
					@if(!empty($partners))
					@foreach($partners as $partner)
					<div class="col-6 col-md-2">
						<a href="#"><img src="/banner-images/{{$partner->image_url}}" alt="" class="img-fluid img-border"></a>
					</div>
					@endforeach
					@endif
					
				</div>
			</div>
		</div>
	</div>

	

@endsection