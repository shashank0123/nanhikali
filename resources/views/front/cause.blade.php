<?php
use App\Donation;
use App\Cause;
?>

@extends('layouts.front')

@section('content')

<!-- BANNER -->
<div class="section banner-page" data-background="/banner-images/NGO-6.png">
	<div class="content-wrap pos-relative">
		<div class="d-flex justify-content-center bd-highlight mb-3">
			<div class="title-page">Causes</div>
		</div>
		<div class="d-flex justify-content-center bd-highlight mb-3">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb ">
					<li class="breadcrumb-item"><a href="/">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">Causes</li>
				</ol>
			</nav>
		</div>
	</div>
</div>

<!-- HOW TO HELP US -->
<div class="section">
	<div class="content-wrap">
		<div class="container">

			<div class="row">

				<div class="rs-nav-tabs">
					<ul class="nav nav-pills mb-3 justify-content-center" id="pills-tab" role="tablist">
						@if(!empty($categories))
						@foreach($categories as $cat)
						<li class="nav-item">
							<a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#cat{{$cat->id}}" role="tab" aria-controls="pills-home" aria-selected="true">{{$cat->name}}</a>
						</li>
						@endforeach
						@endif						  
					</ul>
					<div class="tab-content" id="pills-tabContent">
						@if(!empty($categories))
						@foreach($categories as $cat)

						
						<div class="tab-pane fade show active" id="cat{{$cat->id}}" role="tabpanel" aria-labelledby="pills-home-tab">
							<div class="col-md-12">
								<div class="row">
									@if(!empty($causes))
									@foreach($causes as $cause)

									<?php 
									$donate = Donation::where('project_slug',$cause->slug)->where('status','success')->get();
									$donate_amt = 0;
									$donation_remain = 0;

									if(!empty($donate)){
										foreach($donate as $done){
											$donate_amt = $donate_amt + $done->donated_amount;
										}
									}

									$donation_remain = $cause->targeted_amount - $donate_amt;
									$percent = ($donate_amt*100)/$cause->targeted_amount;
									?>

									<!-- Item 1 -->
									<div class="col-sm-4 col-md-4">
										<div class="box-fundraising">
											<div class="media">
												<img src="/causes-img/{{$cause->image_url}}" alt="" style="width: 100%; height: 250px">
											</div>
											<div class="body-content">
												<p class="title"><a href="/single-cause/{{$cause->slug}}">{{$cause->title}}</a></p>
												<div class="text">{{substr($cause->short_description,0,100)}}..</div>
												<div class="progress-fundraising">
													<div class="total">Rs.{{$donation_remain}} <span>to go</span></div>
													<div class="persen">{{$percent}}%</div>
													<div class="progress">
														<div class="progress-bar" role="progressbar" aria-valuenow="{{$percent}}" aria-valuemin="0" aria-valuemax="100"></div>
													</div>
												</div>
											</div>
										</div>
									</div>
									@endforeach
									@endif




								</div>
							</div>

						</div>
						<!-- END TAB 1 CONTENT -->
						@endforeach
						@endif


					</div>
				</div>					
			</div>

		</div>
	</div>
</div>


@endsection