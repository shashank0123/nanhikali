@extends('layouts.front')

@section('content')

<!-- BANNER -->
<div class="section banner-page" data-background="/banner-images/NGO-6.png">
	<div class="content-wrap pos-relative">
		<div class="d-flex justify-content-center bd-highlight mb-3">
			<div class="title-page">Privacy Policy</div>
		</div>
		<div class="d-flex justify-content-center bd-highlight mb-3">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb ">
					<li class="breadcrumb-item"><a href="/">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">Privacy Policy</li>
				</ol>
			</nav>
		</div>
	</div>
</div>

<!-- WHY CHOOSE US -->
<div class="section">
	<div class="content-wrap">
		<div class="container">
			<h3 style="text-align: center;">{{$content->page_name}}</h3>
			<div class="row">
				<div class="col-sm-1"></div>
				<div class="col-sm-10" style="color: #999999 !important; text-align: justify; padding: 2%">
					


					<?php
					echo $content->content;
					?>

			</div>
			<div class="col-sm-1"></div>



		</div>
	</div>
</div>
</div>





@endsection