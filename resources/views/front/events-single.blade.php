<?php
use App\City;
use App\State;	
?>

@extends('layouts.front')

@section('content')

<!-- BANNER -->
	<div class="section banner-page" data-background="/banner-images/NGO-6.png">
		<div class="content-wrap pos-relative">
			<div class="d-flex justify-content-center bd-highlight mb-3">
				<div class="title-page">{{$event->title}}</div>
			</div>
			<div class="d-flex justify-content-center bd-highlight mb-3">
			    <nav aria-label="breadcrumb">
				  <ol class="breadcrumb ">
				    <li class="breadcrumb-item"><a href="/">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">{{$event->title}}</li>
				  </ol>
				</nav>
		  	</div>
		</div>
	</div>

	<!-- HOW TO HELP US -->
	<div class="section">
		<div class="content-wrap">
			<div class="container">

				<div class="row">

					<div class="col-sm-8 col-md-8">

						<div class="img-date">
							<div class="meta-date">
								<div class="date">02</div>
								<div class="month">AUG</div>
							</div>
							<img src="/events-img/{{$event->image_url}}" alt="" class="img-fluid" style="height: 300px; width: auto">
						</div>

						<div class="spacer-10"></div>

						<h2 class="color-secondary">{{$event->title}}</h2>

						<div class="meta">
							<span class="date"><i class="fa fa-clock-o"></i>  {{$event->timing}}</span>
							<?php
									$city = City::where('id',$event->city)->first();
									$state = State::where('id',$event->state)->first();
									?>
							<span class="location"><i class="fa fa-map-marker"></i> {{$city->city}}, {{$state->state}}</span>
						</div>

						<div class="spacer-30"></div>

						<p class="uk18 color-secondary" style="text-align: justify;"><?php echo $event->description; ?></p>

						<div class="spacer-30"></div>
						{{-- <h3 class="color-secondary">Event  <span class="color-primary">Content</span>?</h3>
						<ol>
							<li>Sed ut perspiciatis unde omnis iste natus error sit voluptatem.</li>
							<li>Accusantium dolore mque laudantium, totam rem aperiam</li>
							<li>Eaque ipsa quae ab illo invent.</li>
							<li>Sed ut perspiciatis unde omnis iste natus error sit.</li>
						</ol>
 --}}
						
						<p class="uk18"><a href="/faq" class="color-primary">NEED HELP? HAVE QUESTIONS?</a></p>
						<div class="spacer-10"></div>

						<div class="rs-box-download">
							<div class="icon">
								<i class="fa fa-file-pdf-o"></i>
							</div>
							<div class="body">
								<a href="#">
									<h3>Download Brochure</h3>
									Click here to download .PDF
								</a>
							</div>
						</div>

					</div>

					<div class="col-sm-4 col-md-4">
						<div class="promo-ads" data-background="/banner-images/volunteer.png">
							<div class="content font__color-white">
								<i class="fa fa-bullhorn"></i>
								<h4 class="title">Become a Volunteer</h4>
								<p class="uk16">We need you now for world</p>
								<p class="font__color-white">You can sponsor the education of one or more underprivileged girls. </p>
								<div class="spacer-30"></div>
								<a data-toggle="modal" data-target="#joinModel" class="btn btn-primary margin-btn">JOIN US NOW</a>
							</div>
						</div>

					</div>

				</div>

			</div>
		</div>
	</div>
	
	

@endsection