@extends('layouts.front')

@section('content')

<!-- BANNER -->
	<div class="section banner-page" data-background="/assets/images/dummy-img-1920x300.jpg">
		<div class="content-wrap pos-relative">
			<div class="d-flex justify-content-center bd-highlight mb-3">
				<div class="title-page">News Sidebar</div>
			</div>
			<div class="d-flex justify-content-center bd-highlight mb-3">
			    <nav aria-label="breadcrumb">
				  <ol class="breadcrumb ">
				    <li class="breadcrumb-item"><a href="/">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">News Sidebar</li>
				  </ol>
				</nav>
		  	</div>
		</div>
	</div>

	<!-- CONTENT -->
	<div class="section">
		<div class="content-wrap">
			<div class="container">
				<div class="row">
					<div class="col-sm-3 col-md-3">
						<div class="widget text">
							<div class="widget-title">
								About Company
							</div>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate.</p>
						</div>
						<div class="widget download">
							<div class="rs-box-download block">
								<div class="icon">
									<i class="fa fa-file-pdf-o"></i>
								</div>
								<div class="body">
									<a href="#">
										<h3>Download Brochure</h3>
										Click here to download .PDF
									</a>
								</div>
							</div>
						</div>
						<div class="widget contact-info-sidebar">
							<div class="widget-title">
								Contact Info
							</div>
							<div class="rs-icon-info">
								<div class="info-icon">
									<i class="fa fa-map-marker"></i>
								</div>
								<div class="info-text">99 S.t Jomblo Park Pekanbaru 28292. Indonesia</div>
							</div>
							<div class="rs-icon-info">
								<div class="info-icon">
									<i class="fa fa-envelope"></i>
								</div>
								<div class="info-text">info@yoursite.com</div>
							</div>
							<div class="rs-icon-info">
								<div class="info-icon">
									<i class="fa fa-phone"></i>
								</div>
								<div class="info-text">(0761) 654-123987</div>
							</div>
						</div> 

					</div>
					<div class="col-sm-9 col-md-9">
						<div class="row list-news">
							<div class="news-item">
								<div class="image">
									<a href="news-detail" title="House Insurance">
										<img src="/assets/images/dummy-img-900x600.jpg" alt="" class="img-fluid">
									</a>
								</div>
								<h2 class="blok-title">
									<a href="news-detail" title="House Insurance">Etiam eleifend ac tellus aliquet congue.</a>
								</h2>
								<div class="meta">
									<div class="meta-date"><i class="fa fa-clock-o"></i> April 25, 2017</div>
									<div class="meta-author"> Posted by: <a href="#">Rome Doel</a></div>
									<div class="meta-category"> Category: <a href="#">Industry</a>, <a href="#">Machine</a></div>
									<div class="meta-comment"><i class="fa fa-comment-o"></i> <a href="#">2 Comments</a></div>
								</div>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>
							</div>
							<div class="news-item">
								<div class="image">
									<a href="news-detail" title="Industrial">
										<img src="/assets/images/dummy-img-900x600.jpg" alt="" class="img-fluid">
									</a>
								</div>
								<h2 class="blok-title">
									<a href="news-detail" title="Industrial">Duis sed quam eget massa porta blandit quis id nibh. Nam ac nulla nunc.</a>
								</h2>
								<div class="meta">
									<div class="meta-date"><i class="fa fa-clock-o"></i> April 25, 2017</div>
									<div class="meta-author"> Posted by: <a href="#">Rome Doel</a></div>
									<div class="meta-category"> Category: <a href="#">Industry</a>, <a href="#">Machine</a></div>
									<div class="meta-comment"><i class="fa fa-comment-o"></i> <a href="#">2 Comments</a></div>
								</div>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>
							</div>
							<div class="news-item">
								<div class="image">
									<a href="news-detail" title="Industrial">
										<img src="/assets/images/dummy-img-900x600.jpg" alt="" class="img-fluid">
									</a>
								</div>
								
								<h2 class="blok-title">
									<a href="news-detail" title="Industrial">Morbi at sodales arcu vitae laoreet urna aenean ac justo nibh.</a>
								</h2>
								<div class="meta">
									<div class="meta-date"><i class="fa fa-clock-o"></i> April 25, 2017</div>
									<div class="meta-author"> Posted by: <a href="#">Rome Doel</a></div>
									<div class="meta-category"> Category: <a href="#">Industry</a>, <a href="#">Machine</a></div>
									<div class="meta-comment"><i class="fa fa-comment-o"></i> <a href="#">2 Comments</a></div>
								</div>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>
							</div>

						</div>
						<div class="row">
							<div class="col-sm-12 col-md-12">
								
								<nav aria-label="Page navigation">
								  <ul class="pagination">
								    <li class="page-item"><a class="page-link" href="#">Previous</a></li>
								    <li class="page-item active"><a class="page-link" href="#">1</a></li>
								    <li class="page-item"><a class="page-link" href="#">2</a></li>
								    <li class="page-item"><a class="page-link" href="#">3</a></li>
								    <li class="page-item"><a class="page-link" href="#">Next</a></li>
								  </ul>
								</nav>

							</div>
						</div>
						
					</div>

				</div>

			</div>
		</div>
	</div>	

@endsection