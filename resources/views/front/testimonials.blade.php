@extends('layouts.front')

@section('content')


	<!-- BANNER -->
	<div class="section banner-page" data-background="/banner-images/NGO-6.png">
		<div class="content-wrap pos-relative">
			<div class="d-flex justify-content-center bd-highlight mb-3">
				<div class="title-page">Testimonials</div>
			</div>
			<div class="d-flex justify-content-center bd-highlight mb-3">
			    <nav aria-label="breadcrumb">
				  <ol class="breadcrumb ">
				    <li class="breadcrumb-item"><a href="/">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Testimonials</li>
				  </ol>
				</nav>
		  	</div>
		</div>
	</div>

	<!-- OUR GALLERY -->
	<div class="section">
		<div class="content-wrap">
			<div class="container">

				<div class="row">
					@if(!empty($testimonials))
					@foreach($testimonials as $test)
					<div class="col-sm-6 col-md-6">
						<div class="testimonial-1">
		              		<div class="media" style="text-align: center; vertical-align: middle;">
	              				<img src="/testimonials-img/{{$test->image_url}}" alt="" class="img-fluid">
	              			</div>
			              	<div class="body">
			                	<p><?php echo $test->review; ?> </p>
			                	<div class="title">{{$test->name}}</div>
			                	<div class="company">{{$test->occupasion}}</div>
			              	</div>
			            </div>
					</div>
					@endforeach
					@endif				

				</div>

				<div class="spacer-50"></div>

				<div class="row justify-content-center">
					<div class="col-sm-12 col-md-10 offset-md-1">
						<div id="testimonial">
							@if(!empty($testimonials))
							@foreach($testimonials as $test)
							<div class="item">
								<div class="rs-box-testimony">
									<div class="quote-box">
										<blockquote  style="text-align: justify;">
										 <?php echo $test->review; ?>
										</blockquote>
										<div class="media">
											<img src="/testimonials-img/{{$test->image_url}}" alt="" class="rounded-circle">
										</div>
										<p class="quote-name">
											{{$test->name}} <span>{{$test->occupasion}}</span>
										</p>                        
									</div>
								</div>
							</div>
							@endforeach
							@endif							

						</div>
					</div>

				</div>

			</div>
		</div>
	</div>
	
@endsection