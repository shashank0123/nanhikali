@extends('layouts.front')

@section('content')

<!-- BANNER -->
	<div class="section banner-page" data-background="/banner-images/NGO-6.png">
		<div class="content-wrap pos-relative">
			<div class="d-flex justify-content-center bd-highlight mb-3">
				<div class="title-page">Frequently Asked Questions</div>
			</div>
			<div class="d-flex justify-content-center bd-highlight mb-3">
			    <nav aria-label="breadcrumb">
				  <ol class="breadcrumb ">
				    <li class="breadcrumb-item"><a href="/">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">FAQ</li>
				  </ol>
				</nav>
		  	</div>
		</div>
	</div>

	<!-- HOW TO HELP US -->
	<div class="section">
		<div class="content-wrap">
			<div class="container">
				<div class="row">					

					<div class="col-sm-8 col-md-8">
						<h2 class="section-heading">
							How To <span>Help</span> Us
						</h2>
						<div class="section-subheading">Project Nanhi Kali is a participatory project where you can sponsor the education of an underprivileged girl child. You can sponsor a Nanhi Kali from primary school studying in class 1-5 at just Rs. 3600 a year, while for Rs. 4800 you can sponsor a Nanhi Kali from secondary school studying in class 6-10. Thereafter, in the first year, you will receive the photograph, profile and progress report of the Nanhi Kali your support, so you are updated on how she is faring in both academics as well as extracurricular activities.</div> 
						<div class="margin-bottom-50"></div>
						<dl class="hiw">
							<dt><span class="fa fa-gift"></span></dt>
							<dd><div class="no">01</div><h3>Send Donation</h3>Please come forward and contribute. Your contribution will transform lives of many.</dd>
							<dt><span class="fa fa-male"></span></dt>
							<dd><div class="no">02</div><h3>Become Volunteer</h3>You can sponsor the education of one or more underprivileged girls. You will have the option to choose the number and education level of the girls you are sponsoring.</dd>
							<dt><span class="fa fa-bullhorn"></span></dt>
							<dd><div class="no">03</div><h3>Share Media</h3>Increase our followers on Twitter, Expand our network on Facebook, Write stories and updates for our Blog & Follow us on Instagram.</dd>
							
						</dl>
						<div class="spacer-50"></div>
						<h2 class="section-heading">
							General <span>Questions</span>
						</h2>
						{{-- <div class="section-subheading">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium dolore mque laudantium, totam rem aperiam, eaque ipsa quae ab illo invent.</div>  --}}
						
						<div class="margin-bottom-50"></div>
						
						<div class="accordion rs-accordion" id="accordionExample">

							@if(!empty($faqs))
							<?php $i=1;?>
							@foreach($faqs as $faq)
							<!-- Item 1 -->
						  <div class="card mb-2">
						    <div class="card-header" id="headingOne">
						      <h4 class="title">
						        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne{{$i}}" aria-expanded="true" aria-controls="collapseOne">
						          {{$faq->questions}}
						        </button>
						      </h4>
						    </div>
						    <div id="collapseOne{{$i}}" class="collapse @if($i==1) {{'show'}} @endif" aria-labelledby="headingOne" data-parent="#accordionExample">
						      <div class="card-body">
						        <?php echo $faq->answers; ?> 
						      </div>
						    </div>
						  </div>
						  <?php $i++; ?>
						  @endforeach
						  @endif

						</div>

					</div>

					<div class="col-sm-4 col-md-4">

						<div class="promo-ads" data-background="/banner-images/volunteer.png">
							<div class="content font__color-white">
								<i class="fa fa-bullhorn"></i>
								<h4 class="title">Become a Volunteer</h4>
								<p class="uk16">We need you now for world</p>
								<p class="font__color-white">You can sponsor the education of one or more underprivileged girls. </p>
								<div class="spacer-30"></div>
								<a href="" class="btn btn-secondary">JOIN US NOW</a>
							</div>
						</div>

						<div class="spacer-30"></div>

						<div class="rs-box-download block">
							<div class="icon">
								<i class="fa fa-file-pdf-o"></i>
							</div>
							<div class="body">
								<a href="#">
									<h3>Download Brochure</h3>
									Click here to download .PDF
								</a>
								<a href="#"><img src="/logo/pdf.png" style="width: 120px; height: auto;"></a>
							</div>
						</div>

					</div>

				</div>
			</div>
		</div>
	</div>

@endsection