@extends('layouts.front')

@section('content')

<!-- BANNER -->
	<div class="section banner-page" data-background="/banner-images/NGO-6.png">
		<div class="content-wrap pos-relative">
			<div class="d-flex justify-content-center bd-highlight mb-3">
				<div class="title-page">{{$cause->title}}</div>
			</div>
			<div class="d-flex justify-content-center bd-highlight mb-3">
			    <nav aria-label="breadcrumb">
				  <ol class="breadcrumb ">
				    <li class="breadcrumb-item"><a href="/">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">{{$cause->title}}</li>
				  </ol>
				</nav>
		  	</div>
		</div>
	</div>

	<!-- HOW TO HELP US -->
	<div class="section">
		<div class="content-wrap">
			<div class="container">

				<div class="row">
					<div class="col-sm-12 col-md-12" style="text-align: center;">
						<img src="/causes-img/{{$cause->image_url}}" alt="" class="img-fluid">
					</div>
				</div>

				<div class="spacer-90"></div>

				<div class="row">
					<div class="col-sm-12 col-md-12">
						{{-- <h2 class="section-heading">
							Urgent <span>Cause</span>
						</h2> --}}

						<h2 class="color-secondary"><span class="color-primary">#{{$cause->title}}</span></h2>
					</div>

					<div class="col-sm-6 col-md-6">

						<p class="uk18 color-secondary" style="text-align: justify;"><?php echo $cause->description; ?></p>
						
						<div class="rs-box-download">
							<div class="icon">
								<i class="fa fa-file-pdf-o"></i>
							</div>
							<div class="body">
								<a href="#">
									<h3>Download Brochure</h3>
									Click here to download .PDF
								</a>
							</div>
						</div>

					</div>

					<div class="col-sm-6 col-md-6">
						<div class="img-video">
							<a href="{{$cause->video_url}}" class="popup-youtube play-video"><i class="fa fa-play fa-2x"></i></a>
							<img src="/banner-images/NGO-8.png" alt="" style="width: 600pc; height: 400px">
							<div class="ripple"></div>
						</div>
							
							<div class="spacer-30"></div>

							<div class="progress-fundraising progress-lg">
		              			<div class="total">Donated</div>
		          			  	<div class="persen">50%</div>
		              			<div class="progress">
								  <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
								</div>
								<div class="detail">
									<h3>Rs.18,500 <small>Raised of</small> Rs.{{$cause->targeted_amount}} <small>Goal</small></h3>
								</div>
							</div>

							<a class="btn btn-lg btn-primary" data-toggle="modal" data-target="#exampleModalLong">DONATE NOW</a>

					</div>

				</div>

			</div>
		</div>
	</div>
	

@endsection