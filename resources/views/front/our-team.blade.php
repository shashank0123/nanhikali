@extends('layouts.front')

@section('content')

<!-- BANNER -->
	<div class="section banner-page" data-background="/banner-images/NGO-6.png">
		<div class="content-wrap pos-relative">
			<div class="d-flex justify-content-center bd-highlight mb-3">
				<div class="title-page">Our Team</div>
			</div>
			<div class="d-flex justify-content-center bd-highlight mb-3">
			    <nav aria-label="breadcrumb">
				  <ol class="breadcrumb ">
				    <li class="breadcrumb-item"><a href="/">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Our Team</li>
				  </ol>
				</nav>
		  	</div>
		</div>
	</div>

	<!-- HOW TO HELP US -->
	<div class="section">
		<div class="content-wrap">
			<div class="container">
				<div class="row">

					@if(!empty($members))
					@foreach($members as $member)
					<div class="col-sm-3 col-md-3">
						<div class="rs-team-1">
							<div class="media"><img src="/team-img/{{$member->image_url}}" alt="" style="width: 400px; height: 250px"></div>
							<div class="body">
								<div class="title">{{$member->member_name}}</div>
								<div class="position">{{strtoupper($member->role)}}</div>
								<ul class="social-icon">
									{{-- <li><a href="#"><span class="fa fa-facebook"></span></a></li>
									<li><a href="#"><span class="fa fa-twitter"></span></a></li>
									<li><a href="#"><span class="fa fa-google-plus"></span></a></li> --}}
								</ul>
							</div>
						</div>
					</div>
					@endforeach
					@endif

				</div>

				<div class="row">

					<div class="cta-info">
						<h1 class="section-heading center">
							Join <span>Our</span> Team?
						</h1>
						<p>Become a member of our team.</p>
						<div class="spacer-50"></div>
						<a href="/about-us" class="btn btnlrn btn-primary margin-btn">LEARN MORE</a> <a  data-toggle="modal" data-target="#joinModel" class="btn btn-primary margin-btn">JOIN US NOW</a>	
					</div>

				</div>

			</div>
		</div>
	</div>
	
@endsection