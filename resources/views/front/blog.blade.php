@extends('layouts.front')

@section('content')

<!-- BANNER -->
	<div class="section banner-page" data-background="/banner-images/NGO-6.png">
		<div class="content-wrap pos-relative">
			<div class="d-flex justify-content-center bd-highlight mb-3">
				<div class="title-page">Blog</div>
			</div>
			<div class="d-flex justify-content-center bd-highlight mb-3">
			    <nav aria-label="breadcrumb">
				  <ol class="breadcrumb ">
				    <li class="breadcrumb-item"><a href="/">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Blog</li>
				  </ol>
				</nav>
		  	</div>
		</div>
	</div>

	<!-- CONTENT -->
	<div class="section">
		<div class="content-wrap">
			<div class="container">
				<div class="row">
					@if(!empty($blogs))
					@foreach($blogs as $blog)
					<div class="col-sm-6 col-md-4">
						<!-- BOX 1 -->
						<div class="box-news-1">
							<div class="media gbr">
								<img src="/blogs-img/{{$blog->image_url}}" alt="" class="img-fluid" style="width: 100%; height: 250px">
							</div>
							<div class="body">
								<div class="title"><a href="/blog-detail/{{$blog->slug}}" title="">{{substr($blog->short_title,0,100)}}.. </a></div>
								<div class="meta">
									<span class="date"><i class="fa fa-clock-o"></i>{{$blog->created_at}}</span>
									<span class="comments"><i class="fa fa-comment-o"></i> 0 Comments</span>
								</div>
							</div>
						</div>
					</div>
					@endforeach
					@endif
					
				
				</div>

				{{-- <div class="row">
					<div class="col-sm-12 col-md-12">
						
						<nav aria-label="Page navigation">
						  <ul class="pagination">
						    <li class="page-item"><a class="page-link" href="#">Previous</a></li>
						    <li class="page-item active"><a class="page-link" href="#">1</a></li>
						    <li class="page-item"><a class="page-link" href="#">2</a></li>
						    <li class="page-item"><a class="page-link" href="#">3</a></li>
						    <li class="page-item"><a class="page-link" href="#">Next</a></li>
						  </ul>
						</nav>

					</div>
				</div> --}}

			</div>
		</div>
	</div>	

    

@endsection