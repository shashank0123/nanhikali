@extends('layouts.front')

@section('content')

<!-- BANNER -->
<div class="section banner-page" data-background="/banner-images/NGO-6.png">
	<div class="content-wrap pos-relative">
		<div class="d-flex justify-content-center bd-highlight mb-3">
			<div class="title-page">Disclaimer</div>
		</div>
		<div class="d-flex justify-content-center bd-highlight mb-3">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb ">
					<li class="breadcrumb-item"><a href="/">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">Disclaimer</li>
				</ol>
			</nav>
		</div>
	</div>
</div>

<!-- WHY CHOOSE US -->
<div class="section">
	<div class="content-wrap">
		<div class="container">
			<h3 style="text-align: center;">Disclaimer</h3>
			<div class="row">
				<div class="col-sm-1"></div>
				<div class="col-sm-10" style="color: #999999 !important; text-align: justify;  padding: 2%">
					

<p>By getting to and perusing the www.nanhi-kali.org site Nanhi Kali Trust ("NKT") or by utilizing or potentially downloading any substance from the equivalent, you concur and acknowledge the Terms of Use as set out beneath. </p>

<h4>Purpose of the web site</h4>

<p>Every one of the materials contained in NKT 's www.nanhi-kali.org site ("Project Nanhi Kali Website") are accommodated instructive purposes just and will not be translated as a business offer, a permit, a warning, trustee or expert connection among you and NKT. No data gave on this site will be viewed as a substitute for your free examination. </p>

<p>The data gave on Project Nanhi Kali Website might be identified with items or administrations that are not accessible in your nation. </p>

<h4>Links to Third-Party Web Sites</h4>

<p>Connections to outsider sites are accommodated comfort just and don't suggest any endorsement or underwriting by KCMET of the connected destinations, regardless of whether they may contain NKT’s or Project Nanhi Kali's logo, in that capacity locales, are outside KCMET's ability to control. Accordingly, NKT can't be considered in charge of the substance of any connected site or any connection contained in that. </p>

<p>You recognize that the encircling Project Nanhi Kali Website or any comparative procedure is disallowed. </p>

<h4>Intellectual Property</h4>

<p>This Project Nanhi Kali Website is the select property of NKT. Any material that it contains, including, yet not constrained to, writings, information, illustrations, pictures, sounds, recordings, logos, symbols or Html code is secured under the licensed innovation laws and remains NKT’s or outsider's property</p>

<p>You may utilize this material for individual and non-business purposes as per the standards administering protected innovation laws. Some other use or adjustment of the substance of Project Nanhi Kali Website without NKT’s earlier composed authorization is disallowed. </p>

<h4>Warranty and Liability</h4>

<p>All materials, including downloadable programming, contained in Project Nanhi Kali Website is given on 'as is' premise and without guarantee of any sort to the degree permitted by the appropriate law. While NKT will utilize sensible endeavors to give solid data through its Project Nanhi Kali Website, NKT does not warrant that this Project Nanhi Kali Website is free of mistakes, blunders and additionally exclusions, infections, worms, Trojan and so forth, or that its substance is suitable for your specific use or state-of-the-art. NKT maintains all authority to change the data whenever without notice. NKT does not warrant any outcomes got from the utilization of any product accessible on this site. You are exclusively in charge of any utilization of the materials contained in this site. </p>

<p>NKT won't be at risk for any roundabout, noteworthy or accidental harms, including yet not constrained to loss of benefits or incomes, business interference, loss of information emerging out of or regarding the utilization, failure to utilize or dependence on any material contained in this site or any connected site. </p>

<h4>Users’ Comments</h4>

<p>NKT does not accept any commitment to screen the data that you may post on its Project Nanhi Kali Website. </p>

<p>You warrant that all data undertakings, records or different connections sent to us ('Material') or remarks other than close to home information, that you may transmit to NKT through the Project Nanhi Kali Website do not encroach protected innovation rights or some other appropriate law. Such data, material or remarks, will be treated as non-private and non-exclusive. By presenting any data or material, you give NKT a boundless and unalterable permit to utilize, execute, appear, change and transmit such data, material or remarks, including any hidden thought, idea or ability. NKT maintains all authority to utilize such data in any capacity it picks. </p>

<h4>Applicable law - Severability</h4>

<p>These Terms of Use are represented by the Indian Law. The courts in Haryana have purview in connection to any case or activity emerging out of, or regarding, the Terms of Use. </p>

<p>In the event that any arrangement of these Terms of Use is held by a court to be illicit, invalid or unenforceable, the rest of the arrangements will stay in full power and impact. </p>

<h4>Modifications of the Terms of Use</h4>

<p>NKT claims all authority to change the Terms of Use under which this Project Nanhi Kali Website is offered whenever and without notice. You will be naturally bound by these changes when you utilize this site, and should occasionally peruse the Terms of Use. </p>

<p>In the wake of having perused and comprehended the disclaimer and copyright see, I, therefore, consent to acknowledge and maintain every one of the terms and conditions as referenced in that at my sole obligation. </p>

<p>I agree.</p>

				</div>
				<div class="col-sm-1"></div>



			</div>
		</div>
	</div>
</div>





@endsection