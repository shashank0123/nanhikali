@extends('layouts.front')

@section('content')

<!-- BANNER -->
<div class="section banner-page" data-background="/banner-images/NGO-6.png">
	<div class="content-wrap pos-relative">
		<div class="d-flex justify-content-center bd-highlight mb-3">
			<div class="title-page">{{$blog->short_title}}</div>
		</div>
		<div class="d-flex justify-content-center bd-highlight mb-3">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb ">
					<li class="breadcrumb-item"><a href="/">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">{{$blog->short_title}}</li>
				</ol>
			</nav>
		</div>
	</div>
</div>

<!-- CONTENT -->
<div class="section">
	<div class="content-wrap">
		<div class="container">
			<div class="row">
				<div class="col-sm-3 col-md-3">

					<div class="widget widget-text">
						<div class="widget-title">
							{{strtoupper($blog->short_title)}}
						</div>
						<p>{{$blog->long_title}}</p>
					</div> 

						{{-- <div class="widget widget-archive">
							<div class="widget-title">
								Archive
							</div>
							<select class="form-control">
								<option>April 2017</option>
								<option>March 2017</option>
								<option>February 2017</option>
								<option>January 2017</option>
							</select>
						</div>  --}}
{{-- 
						<div class="widget tags">
							<div class="widget-title">
								Tags
							</div>
							<div class="tagcloud">
								<a href="#" title="3 topics">construction</a>
								<a href="#" title="1 topic" >industrial</a>
								<a href="#" title="1 topic" >company</a>
								<a href="#" title="4 topics" >development</a>
								<a href="#" title="2 topics" >manufacturing</a>
								<a href="#" title="1 topic" >mechanical</a>
								<a href="#" title="2 topics" >engineering</a>
								<a href="#" title="1 topic" >architecture</a>
								<a href="#" title="2 topics" >planning</a>
								<a href="#" title="5 topics">assembly</a>
								<a href="#" title="2 topics">themeforest</a>
								<a href="#" title="1 topic" >infrastructure</a>
							</div>
						</div> --}}
						

					</div>
					<div class="col-sm-9 col-md-9">
						<div class="single-news">
							<div class="image" style="text-align: center;">
								<img src="/blogs-img/{{$blog->image_url}}" alt="" class="img-fluid"> 
							</div>
							<h2 class="blok-title">
								{{ucfirst($blog->short_title)}}
							</h2>
							<div class="meta">
								<div class="meta-date"><i class="fa fa-clock-o"></i> {{$blog->date}}</div>
								<div class="meta-author"> Posted by: <a href="#">{{$blog->author_name}}</a></div>
								{{-- <div class="meta-category"> Category: <a href="#">Industry</a>, <a href="#">Machine</a></div> --}}
								<div class="meta-comment"><i class="fa fa-comment-o"></i> 2 Comments</div>
							</div>
							<p style="text-align: justify;"><?php echo $blog->description; ?></p>
						</div>

						 {{-- <div class="author-box">
							<div class="media">
								<img src="/assets/images/dummy-img-400x400.jpg" alt="" class="img-fluid">
							</div>
							<div class="body">
	                            <h4 class="media-heading"><span>Author :</span>John Doel</h4>
	                            We are also create and designing template for categories Graphic template and Game asset. in November 2014, we have won big contest Envato most wanted for categories game assets.
	                      </div>
	                      <div class="clearfix"></div>
	                  </div> --}}

	                  <div class="comments-box">
	                  	<h4 class="title-heading">Comments <span>(0)</span></h4>

	                  	{{-- <div class="media comment">
	                  		<img class="mr-3" alt="80x80" src="/assets/images/dummy-img-400x400.jpg" style="width: 64px; height: 64px;">
	                  		<div class="media-body">
	                  			<h5 class="media-heading mt-0 mb-1">Susi Doel<small class="date">August 24, 2014</small>
	                  			</h5> 
	                  			Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
	                  		</div>
	                  	</div>
	                  	<div class="media reply-comment">
	                  		<img class="mr-3" alt="80x80" src="/assets/images/dummy-img-400x400.jpg" style="width: 64px; height: 64px;">
	                  		<div class="media-body">
	                  			<h5 class="media-heading mt-0 mb-1">Susi Doel<small class="date">August 24, 2014</small>
	                  			</h5> 
	                  			Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
	                  		</div>
	                  	</div>
	                  	<div class="media comment">
	                  		<img class="mr-3" alt="80x80" src="/assets/images/dummy-img-400x400.jpg" style="width: 64px; height: 64px;">
	                  		<div class="media-body">
	                  			<h5 class="media-heading mt-0 mb-1">Susi Doel<small class="date">August 24, 2014</small>
	                  			</h5> 
	                  			Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
	                  		</div>
	                  	</div> --}}

	                  </div>

	                  <div class="leave-comment-box">
	                  	<h4 class="title-heading">Leave Comments</h4>
	                  	<form class="form-comment" id="review-form">
	                  		<!-- CSRF Token -->
	                  		<input type="text" name="blog_id" value="{{$blog->id}}">
	                  		@csrf
	                  		
	                  		<div class="row">
	                  			<div class="col-xs-12 col-md-6">
	                  				<div class="form-group">
	                  					<input type="text" id="name" name="name" value="" class="inputtext form-control" placeholder="Enter Name" required>
	                  				</div>
	                  			</div>
	                  			<div class="col-xs-12 col-md-6">
	                  				<div class="form-group">
	                  					<input type="email" id="name" name="email" value="" class="inputtext form-control" placeholder="Enter Email" required>
	                  				</div>
	                  			</div>
	                  			
	                  			<div class="col-xs-12 col-md-12">
	                  				<div class="form-group">
	                  					<textarea id="message" name="message" class="inputtext form-control" rows="6" placeholder="Enter Your Message..."></textarea>
	                  				</div>
	                  			</div>
	                  			<div class="col-xs-12 col-md-12">
	                  				<div class="form-group">
	                  					<button type="button" class="btn btn-primary" onclick="sendComment()">Post Comment</button>
	                  				</div>
	                  			</div>
	                  		</div>
	                  	</form>

	                  </div>

	              </div>

	          </div>

	      </div>
	  </div>
	</div>	

	

	@endsection