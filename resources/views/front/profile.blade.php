<?php
use App\Address;
use App\City;
use App\State;

$address = Address::where('user_id',Auth::user()->id)->first();

?>


@extends('layouts.front')

@section('content')
<style>
  #donation,#password{ display: none; }
  #profile {  border : 1px solid #ccc; }
  #row-left{ border-right: 1px solid #ccc; }
  #row-left ul { background-color: #9D6C34 ; color: #fff; cursor: pointer; font-size: 18px; margin: 15px 5px; }
  #row-left a { color: #fff; }
  #row-left li { padding: 20px; border-bottom: 1px solid #fff }
  #personal  { padding-bottom: 20px }
  #result-info { display: none; }
  #result-password { display: none; }
</style>
<br><br>
<div class="container">
  <div class="row" id="profile">
    <div class="col-sm-3" id="row-left">
      <ul>
        <li onclick="showProfile()">Profile</li>
        <li onclick="showDonation()">Donation History</li>
        <li onclick="showPassword()">Reset Password</li>
        <li><a href="{{ route('logout') }}"
          onclick="event.preventDefault();
          document.getElementById('logout-form').submit();">
          Logout
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          @csrf
        </form></li>
      </ul>
    </div>
    <div class="col-sm-9">
      <div class="container">

        <div class="row" id='personal'>
          <form class="container" id="update-account">
            @csrf
            <h3>Personal Information</h3>
            <p>(Your can update your profile from here.)</p>
            <div class="col-sm-12" id="personal">
              <div class="row">
                <div class="col-sm-3">
                  <label>Name</label>
                </div>
                <div class="col-sm-9">
                  <input type="text" name="username" class="form-control" value="{{Auth::user()->name}}"/>
                </div>
              </div>              
            </div>

            <div class="col-sm-12" id="personal">
              <div class="row">
                <div class="col-sm-3">
                  <label>Email</label>
                </div>
                <div class="col-sm-9">
                  <input type="email" name="email"  class="form-control" value="{{Auth::user()->email}}" readonly />
                </div>
              </div>              
            </div>

            <div class="col-sm-12" id="personal">
              <div class="row">
                <div class="col-sm-3">
                  <label>Phone</label>
                </div>
                <div class="col-sm-9">
                  <input type="phone" name="phone"  class="form-control" value="{{Auth::user()->phone}}"/>
                </div>
              </div>              
            </div>

            <div class="col-sm-12" id="personal">
              <div class="row">
                <div class="col-sm-3">
                  <label>Address</label>
                </div>
                <div class="col-sm-9">
                  <input type="text" name="address"  class="form-control" value="@if(!empty($address)){{$address->address}}@endif"/>
                </div>
              </div>              
            </div>

            <div class="col-sm-12" id="personal">
              <div class="row">
                <div class="col-sm-3">
                  <label>State</label>
                </div>

                <?php
                
                
                ?>
                <div class="col-sm-9">
                  <select id="state" name="state" class="form-control">
                          <option>- Select State -</option>
                          @foreach($states as $state)
                          <option value="{{$state->id}}" @if(!empty($address->state)) @if($address->state == $state->id) {{'selected'}}  @endif @endif>{{$state->state}}</option>
                          @endforeach
                        </select>
                </div>
              </div>              
            </div>

            <div class="col-sm-12" id="personal">
              <div class="row">
                <div class="col-sm-3">
                  <label>City</label>
                </div>
                <div class="col-sm-9">
                  <select id="city" name="city" class="form-control">
                          @if(!empty($address->city))
                          <?php
                          $city = City::where('id',$address->city)->first();
                          echo "<option value='".$city->id."'>".$city->city."</option>";
                          ?>

                          @else
                          <option>- Select City -</option>
                          @endif
                        </select>
                </div>
              </div>              
            </div>

            <div class="col-sm-12" style="text-align: center;">
              <button type="button" name="update" class="btn btn-danger" style="background-color: #9D6C34" onclick="updateInfo()">Update</button>
              <button type="button" name="cancel" class="btn btn-danger" style="background-color: #3f3f3f">Cancel</button>
            </div>
            <br><br>

            <div style="text-align: center;" class="btn btn-success" id="result-info">
              
            </div>

          </form>
        </div>

        <div class="row" id="donation">
          <h3>Donation History</h3>
          <table class="table">
            <thead>
              <th>Date</th>
              <th>Amount</th>
              <th>Payment Method</th>
              <th>Transaction ID</th>
            </thead>
            <tbody>
              <td>12-12-2000</td>
              <td>ABC</td>
              <td>payumoney</td>
              <td>asd324sf2edsc</td>
            </tbody>
          </table>
        </div>

        <div class="row" id="password">
          <h3>Reset Your Password</h3>
          <form id="reset-password">
            @csrf

            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
            <div clas="row">
              <div class="col-sm-4">
                <label>Current Password</label>
              </div>
              <div class="col-sm-8">
                <input type="text" name="old_password" class="form-control">
              </div>
            </div>

            <div clas="row">
              <div class="col-sm-4">
                <label>New Password</label>
              </div>
              <div class="col-sm-8">                
                <input type="password" name="new_password" class="form-control">
              </div>
            </div>

            <div clas="row">
              <div class="col-sm-4">
                <label>Re-enter New Password </label>
              </div>
              <div class="col-sm-8">
                <input type="password" name="re_password" class="form-control">              
              </div>
            </div>
            <br>

            <div class="row" style="text-align: center;">
              <button type="button" name="reset" class="btn btn-danger" style="background-color: #9D6C34" onclick="updatePassword()">Submit</button>
              <button type="button" name="cancel" class="btn btn-danger" style="background-color: #3f3f3f">Cancel</button>
            </div>

            <br><br>

            <div style="text-align: center;" class="btn btn-success" id="result-password">
              
            </div>
            
          </form>
        </div>

      </div>
    </div>
  </div>
</div>
<br><br>
<script>
  function showDonation(){
    $('#personal').hide();
    $('#password').hide();
    $('#donation').show();
  }

  function showPassword(){
    $('#personal').hide();
    $('#donation').hide();
    $('#password').show();
  }

  function showProfile(){
    $('#donation').hide();
    $('#password').hide();
    $('#personal').show();
  }

  function updateInfo(){
    $.ajax({              
          url: '/update-info',
          type: 'POST',             
          data: $('#update-account').serialize(),
          success: function (data) { 
            if(data.message == 'Updated Successfully'){
          
              $('#result-info').show();
              $('#result-info').html(data.message);
              
            }          
            else{
              $('#result-info').show();
              $('#result-info').html('Something Went Wrong');
            }
          },
          failure:function(data){
            alert(data.message);
          }
        }); 
  }

  function updatePassword(){
    $.ajax({              
          url: '/update-password',
          type: 'POST',             
          data: $('#reset-password').serialize(),
          success: function (data) { 
            if(data.message == 'Updated Successfully'){
            
              $('#result-password').show();
              $('#result-password').html(data.message);
              
            }          
            else{
              $('#result-password').show();
              $('#result-password').html(data.message);
            }
          },
          failure:function(data){
            alert(data.message);
          }
        }); 
  }

  $('#state').on('change',function(){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var state = $('#state').val();

        $.ajax({
          type: 'POST',
          url: '/cities',
          data: {_token: CSRF_TOKEN, state_id : state },
          success:function(data){
            $('#city').html(data);
          }
        });
      });

</script>
@endsection