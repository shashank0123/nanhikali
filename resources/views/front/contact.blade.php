@extends('layouts.front')

@section('content')
<style>
	#success-contact{ display: none; }
</style>
<!-- BANNER -->
<div class="section banner-page" data-background="/banner-images/NGO-6.png">
	<div class="content-wrap pos-relative">
		<div class="d-flex justify-content-center bd-highlight mb-3">
			<div class="title-page">Contact Us</div>
		</div>
		<div class="d-flex justify-content-center bd-highlight mb-3">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb ">
					<li class="breadcrumb-item"><a href="/">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">Contact Us</li>
				</ol>
			</nav>
		</div>
	</div>
</div>

<!-- CONTENT -->
<div class="section">
	<div class="content-wrap">
		<div class="container">
			<div class="row">
				
				<div class="col-sm-8 col-md-8">
					<!-- MAPS -->
					<div class="maps-wraper">
							{{-- <div id="cd-zoom-in"></div>
							<div id="cd-zoom-out"></div> --}}
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7011.760612840188!2d76.98528832341411!3d28.51324867985985!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d10caf0df03db%3A0xd77c026ab3fba99e!2sClub%20Chorus%20%40%20The%20Heartsong!5e0!3m2!1sen!2sin!4v1567058364275!5m2!1sen!2sin" width="100%" height="400px" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
							{{-- <div id="maps" class="maps" data-lat="-7.452278" data-lng="112.708992" data-marker="https://goo.gl/maps/wbnbLMDPKYs6iqt76"> --}}

							</div>
						</div>
						<div class="spacer-90"></div>
						

						<div class="col-sm-4 col-md-4">
							<h2 class="section-heading">
								Contact Details
							</h2>

							<div class="rs-icon-info">
								<div class="info-icon">
									<i class="fa fa-map-marker"></i>
								</div>
								<div class="info-text">
									Experion heartsong<br>
									Sector 108<br>
									A1/506<br>
									Dwarka Express Way<br>
								Gurugram.. 122017  </div>
							</div>

							<div class="rs-icon-info">
								<div class="info-icon">
									<i class="fa fa-envelope"></i>
								</div>
								<div class="info-text"><a href="mailto:info@nanhi-kali.com">info@nanhi-kali.com</a></div>
							</div>

							<div class="rs-icon-info">
								<div class="info-icon">
									<i class="fa fa-phone"></i>
								</div>
								<div class="info-text">
									<a href="tel:+91 97287 73730">+91 97287 73730</a><br>
									<a href="tel:+91-9354149738">+91-9354149738</a><br>

									<a href="tel:+91-9591804800">+91-9591804800</a>
									
								</div>
							</div>

							
						</div>

						<div class="clearfix"></div>
						
						<div class="col-sm-12 col-md-12">
							<h2 class="section-heading">
								Send a <span>Message</span>
							</h2>
							<div class="section-subheading">For any query , write below and send us. We will help you.</div>
							<div class="margin-bottom-50"></div>

							<div class="content">
								<form action="#" class="form-contact" id="contact-Form">
									@csrf
									<div class="row">
										<div class="col-sm-6 col-md-6">
											<div class="form-group">
												<input type="text" class="form-control" id="c_name"  name="name" placeholder="Enter Name" required>
												<div class="help-block with-errors"></div>
											</div>
										</div>
										<div class="col-sm-6 col-md-6">
											<div class="form-group">
												<input type="email" class="form-control" name="email" id="c_email" placeholder="Enter Email" required>
												<div class="help-block with-errors" id="check_email"></div>
											</div>
										</div>
										<div class="col-sm-6 col-md-6">
											<div class="form-group">
												<input type="text" class="form-control" name="subject" id="c_subject" placeholder="Subject" required>
												<div class="help-block with-errors"></div>
											</div>
										</div>
										<div class="col-sm-6 col-md-6">
											<div class="form-group">
												<input type="phone" class="form-control" name="phone" id="c_phone" placeholder="Enter Phone Number" required onchange="validateMobile()">
												<div id="validate-p"></div>
												<div class="help-block with-errors" ></div>
											</div>
										</div>
									</div>
									<div class="form-group">
										<textarea id="p_message" name="message" class="form-control" rows="6" placeholder="Enter Your Message" required></textarea>
										<div class="help-block with-errors"></div>
									</div>
									<div class="form-group">
										<div id="success"></div>
										<button type="button" class="btn btn-primary" onclick="submitContact()">SEND MESSAGE</button>
									</div>
								</form>
								<div style="background-color: #9D6C34; color: #fff; padding: 20px; width: 100%; text-align: center;" id="success-contact"></div>
								<div class="margin-bottom-50"></div>
								{{-- <p><em>Note: Consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</em></p> --}}
							</div>
						</div>
					</div>

				</div>
				
			</div>
		</div>
		

		@endsection

		@section('script')

		<script>

			function validateMobile(){
				var phone = $('#c_phone').val();
				if(phone.length == 10){
					$('#validate-p').hide();
				}
				else{
					$('#validate-p').show();
					$('#validate-p').html('Invalid Mobile Number Format');	
				}
			}


			function submitContact(){
				$.ajax({							
					url: '/submit-contact',
					type: 'POST',							
					data: $('#contact-Form').serialize(),
					success: function (data) { 
						if(data.message == 'Thank you. We will respond you shortly.'){	
							$('#success-contact').show();					
							$('#success-contact').html(data.message);					
						}          
						else{
							alert(data.message);
						}
					},
					failure:function(data){
						alert(data.message);
					}
				}); 
			}

		</script>

		@endsection