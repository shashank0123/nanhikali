@extends('layouts.front')

@section('content')


<!-- BANNER -->
	<div class="section banner-page" data-background="/banner-images/NGO-6.png">
		<div class="content-wrap pos-relative">
			<div class="d-flex justify-content-center bd-highlight mb-3">
				<div class="title-page">Image Gallery
					<br>
					<p style="font-size: 14px">Catch a glimpse of the candid and innocent moments in the lives of our Nanhi Kalis.</p>
				</div>
			</div>
			<div class="d-flex justify-content-center bd-highlight mb-3">
			    <nav aria-label="breadcrumb">
				  <ol class="breadcrumb ">
				    <li class="breadcrumb-item"><a href="/">Home</a></li>
				    <li class="breadcrumb-item active" aria-current="page">Images</li>
				  </ol>
				</nav>
		  	</div>
		</div>
	</div>
 	

	<!-- OUR GALLERY -->
	<div class="section">
		<div class="content-wrap">
			<div class="container">

				<div class="row">
					<div class="col-sm-12 col-md-12">
						<div class="row popup-gallery gutter-5">
							@if(!empty($galleries))
							@foreach($galleries as $gallery)
							<!-- ITEM 1 -->
							<div class="col-xs-6 col-md-3">
								<div class="box-gallery">
									<a href="/banner-images/{{$gallery->image_url}}" title="Gallery #1">
										<img src="/banner-images/{{$gallery->image_url}}" alt="" class="img-fluid" style="width: 100%; height: 200px">
										<div class="project-info">
											<div class="project-icon">
												<span class="fa fa-search"></span>
											</div>
										</div>
									</a>
								</div>
							</div>
							@endforeach
							@endif							
							
						</div>
					</div>

				</div>

			</div>
		</div>
	</div>
	
	
@endsection
