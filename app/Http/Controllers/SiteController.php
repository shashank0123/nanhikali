<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cause;
use App\CauseCategory;
use App\Testimonial;
use App\Blog;
use Validator;
use App\Faq;
use App\Video;
use App\BlogReview;
use App\Newsletter;
use App\Event;
use App\ContactForm;
use App\Image;
use App\State;
use App\City;
use App\Cms;
use App\OurTeam;
use App\Donation;
use App\Award;
use App\Volunteer;
use App\User;
use App\Address;
use App\Emi;
use Mail;
use Hash;
use StdClass;
use Log;

class SiteController extends Controller
{
    public function getIndex(){
        $top_causes = Cause::where('status','Active')->orderBy('created_at','DESC')->where('cause_status','in_process')->limit(3)->get();
        $urgent_cause = Cause::where('status','Active')->where('urgent','yes')->where('cause_status','in_process')->first();
        $testimonials = Testimonial::where('status','Active')->get();
        $upcoming_events = Event::where('status','present')->orderBy('created_at','DESC')->limit(2)->get();
        $banners = Image::where('image_category','banner')->where('status','Active')->get();

        $galleries = Image::where('image_category','gallery')->where('status','Active')->orderBy('id','DESC')->limit(6)->get();
        $partners = Image::where('image_category','partner')->where('status','Active')->orderBy('id','DESC')->limit(6)->get();
        
        return view('front.index',compact('top_causes','urgent_cause','testimonials','upcoming_events','banners','galleries','partners'));
    }

    public function getAbout(){
        $testimonials = Testimonial::where('status','Active')->orderBy('created_at','DESC')->limit(4)->get();
        $partners = Image::where('image_category','partner')->where('status','Active')->orderBy('id','DESC')->limit(6)->get();
        $faqs = Faq::where('status','Active')->get();

        $content = Cms::where('keyword','about_us')->first();

        return view('front.about',compact('content','testimonials','partners'));
    }

    public function getBlog(){
        $blogs = Blog::where('status','Active')->orderBy('created_at','DESC')->get();
        return view('front.blog',compact('blogs'));
    }

    public function getBlogDetail($slug){
        $blog = Blog::where('slug',$slug)->first();
        return view('front.blogdetail',compact('blog'));
    }

    public function getCauseSingle($slug){
        $cause = Cause::where('slug',$slug)->first();
        return view('front.cause-single',compact('cause'));
    }

    public function getAccount(Request $request){

        $states = State::all();
        return view('front.profile',compact('states'));
    }

    public function getCause(){
        $categories = CauseCategory::where('status','Active')->get();
        $causes = Cause::where('status','Active')->get();
        return view('front.cause' , compact('causes','categories'));
    }

    public function getContact(){
    	return view('front.contact');
    }

    public function getEventsSingle($slug){
        $event = Event::where('slug',$slug)->first();
        return view('front.events-single',compact('event'));
    }

    public function getEvents(){
        $events = Event::where('status','future')->orWhere('status','present')->orderBy('created_at','DESC')->get();
        return view('front.events',compact('events'));
    }

    public function getAwards(){
        $awards = Award::where('status','Active')->orderBy('created_at','DESC')->get();
        return view('front.award',compact('awards'));
    }

    public function getAwardDetail($slug){
        $award = Award::where('slug',$slug)->first();
        return view('front.award-single',compact('award'));
    }

    public function getFaq(){
        $faqs = Faq::where('status','Active')->get();
        return view('front.faq',compact('faqs'));
    }

    public function getGallery(){
        $galleries = Image::where('image_category','gallery')->where('status','Active')->orderBy('id','DESC')->get();

        return view('front.gallery',compact('galleries'));
    }

    public function getVideos(){
        $videos = Video::where('status','Active')->orderBy('id','DESC')->get();

        return view('front.videos',compact('videos'));
    }


    public function getNewsSidebar(){
    	return view('front.news-sidebar');
    }

    public function getPrivacyPolicy(){
        $content = Cms::where('keyword','privacy_policy')->first();
        return view('front.privacy-policy',compact('content'));
    }

    public function getDisclaimer(){
        $content = Cms::where('keyword','disclaimer')->first();
        return view('front.disclaimer',compact('disclaimer'));
    }

    public function getOurTeam(){
        $members = OurTeam::where('status','Active')->get();
        return view('front.our-team',compact('members'));
    }

    public function getTestimonials(){
        $testimonials = Testimonial::where('status','Active')->get();
        return view('front.testimonials',compact('testimonials'));
    }

    public function submitReviewForm(Request $request){
        $review = new BlogReview;

        $review->name = $request->name;
        $review->email = $request->email;
        $review->website = $request->website;
        $review->message = $request->message;

        $review->save();
    }

    public function submitDonationInfo(Request $request){

        $response = new StdClass;
        $response->message = "Something Went Wrong";
        $response->status = 200;
        $id = 0;

        $donate = new Donation;
        $user = new User;
        $address = new Address;
        $password = 'asd123';

        $check_user = User::where('email',$request->email)->first();

        if($check_user){
            $id = $check_user->id;
        }

        else{
            $user->name = $request->vendor_name;            
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->password = Hash::make($password);

            $user->type = 'user';

            $user->save();
            $email = $user->email;
            $name = $user->name;

            $data = array('Email'=>$user->email, 'Name' => $user->name, 'Password' => 'asd123');
            Mail::send('front.registration-mail', $data, function($message) use ($email, $name, $password)
            {   
                $message->from('no-reply@nanhi-kali.org', 'No-Reply');
                $message->to($user->email, $user->name)->subject('Registration Successful');
            });

            $id = $user->id;
        }

        $check_address = Address::where('user_id',$id)->first();

        if($check_address){
        }
        else{
            $address->user_id = $id ;
            $address->city = $request->city ;
            $address->state = $request->state ;
            $address->address = $request->address ;
            $address->pin_code = $request->postel_code ;

            $address->save();            
        }

        $donate->project_slug = $request->project_slug;
        $donate->donated_amount = $request->donated_amount;
        $donate->donar_type = $request->donar_type;
        $donate->donation_type = $request->donation_type;
        $donate->user_id = $id;
        $donate->email = $request->email;
        $donate->phone = $request->phone;

        $donate->save();

        if($request->donation_type == 'emi'){
            $emi = new Emi;
            $emi->user_id = $id;
            $emi->pending_amount = $request->donated_amount;
            $emi->installment = $request->installment;
            $emi->project_id = $request->project_slug;
            $emi->donation_id = $donate->id;
            $emi->date = date('Y-m-d');
            $emi->time = '25';

            $emi->save();
        }

        if($donate){
            $response->message = 'Inserted Successfully';
            $response->status = 201;
        }

        return response()->json($response);
    }




    public function donationForm(Request $request){
        $response = new StdClass;
        $response->message = 'Something Went Wrong';
        $response->id = 0;
        $response->status = 200;
        $check = Donation::where('email',$request->emailvalue)->where('phone',$request->phonevalue)->where('project_slug',$request->projectslug)->where('status','pending')->orderBy('created_at','DESC')->first(); 

        if($check){
            $check->payment_mode = $request->payment_mode;
            $check->update();

            $response->message = "Updated Successfully";
            $response->id = $check->id;
            $response->status = 201;
        }

        return response()->json($response);

    }

    public function getPaymentForm($id){
        $donar = Donation::where('id',$id)->orderBy('created_at','DESC')->first();

        return view('front.payment',compact('donar'));
    }

    public function checkoutForm1(Request $request,$id){
        $success = "";
        if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') == 0){
    //Request hash
            $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';   
            if(strcasecmp($contentType, 'application/json') == 0){
                $data = json_decode(file_get_contents('php://input'));
                $hash=hash('sha512', $data->key.'|'.$data->txnid.'|'.$data->amount.'|'.$data->pinfo.'|'.$data->fname.'|'.$data->email.'|||||'.$data->udf5.'||||||'.$data->salt);
                $json=array();          
                $json['success'] = $hash;
                echo json_encode($json);        
            }
            exit(0);
        }

    } 

    public function getSuccess(Request $request){

       

        $check = Donation::where('status','pending')->where('email',$_POST['email'])->where('project_slug',$_POST['productinfo'])->first();

       

        $email = $_POST['email'];
        $txnid = $_POST['txnid'];
        $name = $_POST['firstname'];
        $amount = $_POST['amount'];
        $phone = $_POST['phone'];
        $status = 'Success';

        if(!empty($check)){
           if($check->donation_type == 'emi'){
            $emi = Emi::find($check->id);

            if(!empty($emi)){
                $emi->transaction_id = $_POST['txnid'];
                $emi->status = 'done';

                $emi->update();
            }
        }
    }
    else {
       $check->transaction_id = $_POST['txnid'];
       $check->status = 'done';
       $check->update();
   }

   $data1 = array('Email'=>$email, 'TransactionID'=>$txnid, 'Payment_Status'=>$status);
   Mail::send('front.mail', $data1, function($message) use ($email, $txnid, $status)
   {   
    $message->from('no-reply@nanhi-kali.org', 'No-Reply');
    $message->to($email, 'User')->subject('Successful Donation');
});

   $data2 = array('Email'=>$email, 'TransactionID'=>$txnid, 'Payment_Status'=>$status);
   Mail::send('front.mail2', $data2, function($message) use ($email, $txnid, $status)
   {   
    $message->from('no-reply@nanhi-kali.org', 'No-Reply');
    $message->to('admin@nanhi-kali.org', 'Admin')->subject('New Donation');
});



   return view('front.success',compact('name','phone','txnid','amount'));
}   

public function validatePhone(Request $request){

    $rules = array('phone' => 'required|regex:/^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,3})$/');
    $validator = Validator::make(\Input::all(), $rules);
    if ($validator->fails())
    {
        return \Response::json([
            'success' => 'false',
            'type' => 'error',
            'message' => $validator->getMessageBag()->toArray()
        ]); 
    }        
} 

public function validateEmail(Request $request){

    $rules = array('email' => 'required|email');
    $validator = Validator::make(\Input::all(), $rules);
    if ($validator->fails())
    {
        return \Response::json([
            'success' => 'false',
            'type' => 'error',
            'message' => $validator->getMessageBag()->toArray()
        ]); 
    }        
}

public function submitNewsletter(Request $request){
    $news = new Newsletter;

    $news->email = $request->email;
    $news->status = "Active";

    $news->save();
    if (!empty($news))
    {
        return \Response::json([
            'success' => 'false',
            'type' => 'error',
            'message' => 'Thank you. We will respond you shortly.'
        ]); 
    } 

}

public function submitContactForm(Request $request){
    $contact = new ContactForm;

    $contact->name = $request->name;
    $contact->email = $request->email;
    $contact->phone = $request->phone;
    $contact->subject = $request->subject;
    $contact->message = $request->message;
    $contact->status = "Active";

    $contact->save();

    if (!empty($contact))
    {
        return \Response::json([
            'success' => 'false',
            'type' => 'error',
            'message' => 'Thank you. We will respond you shortly.'
        ]); 
    } 
}

public function submitVolunteerForm(Request $request){
    $vol = new Volunteer;

    $name = $request->volunteer_name;
    $email = $request->email;

    $vol->name = $request->volunteer_name;
    $vol->email = $request->email;
    $vol->phone = $request->phone;

    $vol->message = $request->message;
    $vol->status = "Active";

    $vol->save();        

    if (!empty($vol))
    {

        $content = array('Email'=>$email, 'Name' => $name);
        Mail::send('front.mail', $content, function($message) use ($email, $name)
        {   
            $message->from('jyotikasethi3007@gmail.com', 'Jyotika Sethi');
            $message->to($email, $name)->subject('Thanks to Volunteer');
        });


        return \Response::json([
            'success' => 'false',
            'type' => 'error',
            'message' => 'Welcome, We will respond you shortly.'
        ]); 
    } 

}

public function getCities(Request $request){

    $cities = City::where('state_id',$request->state_id)->get();

    echo "<option>Select City</option>";
    foreach($cities as $city){
        echo "<option value='" . $city->id . "'>" . $city->city . "</option>";          
    }
}


public function updateAccount(Request $request){
    $user = User::where('email',$request->email)->first();

    $user->name = $request->username;
    $user->email = $request->email;
    $user->phone = $request->phone;
    $user->update();

    $address = Address::where('user_id',$user->id)->first();

    $address->address = $request->address; 
    $address->state = $request->state; 
    $address->city = $request->city;

    $address->update();

    if($user && $address ){
        return \Response::json(['message' => 'Updated Successfully']);
    }


}
public function updatePassword(Request $request){

    $user = User::find($request->user_id);

    if($user){
        if($request->new_password == $request->re_password){
            $user->password = Hash::make($request->new_password);

            $user->update();
            return \Response::json(['message' => 'Updated Successfully']);

        }
        else{
            return \Response::json(['message' => 'Password Did Not Match']);
        }
    }

    else{
        return \Response::json(['message' => 'User Not Found']);
    }
}
}
