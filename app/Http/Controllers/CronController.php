<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Donation;
use App\User;
use App\Emi;

class CronController extends Controller
{
	public function get(){

		$donation = Donation::where('status','pending')->where('donation_type','emi')->get();

		if(!empty($donation)){

			foreach ($donation as $done)
			{

				$emis = Emi::where('donation_id',$done->id)->sum('installment');

				if($emis < $done->donated_amount){
					$user = User::where('id',$done->user_id)->first();
					$emi = Emi::where('donation_id',$done->id)->orderBy('created_at','DESC')->first();
					if(!empty($emi)){
						$amount = $emi->installment;
						$product = $done->project_slug;
						$name = $user->name;
						$email = $user->email;

						$message = 'Hey '.strtoupper($name).', you have donate for project '.strtoupper($project).' by donation type EMI. Your next EMI is comming soon. So please vist www.nanhi-kali.org o pay your next installment of Rs. '.$amount.;

						$data = array('Email'=>$user->email, 'Name' => $user->name, 'Amount' => $emi->installment);
						Mail::raw($message, $data, function($message) use ($amount, $product, $password)
						{
							$message->from('no-reply@nanhi-kali.org','Nanhi Kali');
							$message->to($email,$name)->subject('Emi of Donation');
						});
					}

					


				}
			}

		}
	}
}
